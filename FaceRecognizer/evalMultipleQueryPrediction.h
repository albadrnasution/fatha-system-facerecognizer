
#include "stdafx.h"
#include <vector>
#include "multi_prediction.h"
#include "BimaIlumi.h"
#include "Combinatorics.h"
namespace cfr = customfacerec;


using namespace std;
using namespace cv;
class evalMultipleQueryPrediction{

	BimaIlumi aisl;
	string log_name;


	void streamline(vector<vector<Mat>> source, vector<Mat> &images, vector<int> &labels){
		int nPerson = (int) source.size();

		for (int p=0; p < nPerson; ++p){
			vector<Mat> satp = source.at(p);
			for (Mat m : satp){
				//push training image
				images.push_back(m);
				labels.push_back(p);
			}
		}
	}

public:
	static const double SET_DEVBREAK(){return 0.05;}
	const double STDEVBREAK(){ return CSTM_DEVBREAK < 0 ? SET_DEVBREAK() : CSTM_DEVBREAK; }

	//settings
	bool SET_3PRED;
	bool SET_PRINT_PREFSIZE;
	String SET_PREFIX;
	double CSTM_DEVBREAK;

	int* tested_mode;
	int N_TESTED_MODE;

	int modeForStdevCheck;

	evalMultipleQueryPrediction(){
		SET_3PRED = false;
		SET_PRINT_PREFSIZE = false;
		SET_PREFIX = "";
		modeForStdevCheck = 1;

		CSTM_DEVBREAK = -1;

		N_TESTED_MODE = cfr::multi_prediction::N_MODE;
		tested_mode = new int[N_TESTED_MODE];
		for (int i=0; i < N_TESTED_MODE; ++i)
			tested_mode[i] = i;
	}

	~evalMultipleQueryPrediction(){
		delete[] tested_mode;
	}


	void resizeImages(vector<vector<Mat>>  &images, int im_width, int im_height);
	void resizeImages(map<int, vector<Mat>>  &images, int im_width, int im_height);
	void resizeImages(map<int, vector<vector<Mat>>>  &images, int im_width, int im_height);

	vector<vector<Mat>> loadImages(string folder, bool ilNorm, Size resizeTo);
	map<int, vector<Mat>> loadImagesMap(string folder, bool ilNorm, Size resizeTo);
	map<int, vector<vector<Mat>>> loadImagesMapQuery(string folderPath, bool ilNorm, Size resizeTo);

	bool eval_one(cfr::multi_prediction mult, vector<Mat> certainPerson_testSet, int true_label);

	void eval_random(string folder, int ntrain, int ntest, int ntot, int nexp);
	void eval_random(vector<vector<Mat>> images, int ntrain, int ntest, int nexp);
	void separate_random(vector<vector<Mat>> images, int ntrain, int ntest, vector<vector<Mat>> &trainingSet, vector<vector<vector<Mat>>> &testSet);
	void separate_random(vector<vector<Mat>> images, int ntrain, int ntest, vector<vector<Mat>> &trainingSet, vector<vector<Mat>> &remains);


	void separate_combi(string trainBitmask, vector<vector<Mat>> images, vector<vector<Mat>> &image_test, vector<Mat> &image_train, vector<int> &label_train);
	void eval_allcombi(String folder, int nImagePerPerson, int ntrain, int ntest);
	void eval_allcombi(vector<vector<Mat>> images_ori, int nImagePerPerson, int ntrain, int ntest);


	void separate_online(map<int, vector<Mat>> mapList, vector<Mat> &image, vector<int> &label);
	void eval_online(string folderTrain, string folderQuery, int delay, int SIZE=64);
	void eval_online(map<int, vector<Mat>> trains_ori,map<int, vector<vector<Mat>>> queries_ori, int delay);
	void online_generate_queryDelay(vector<Mat> input, int delay, vector<vector<Mat>> &output);


	void iluminateAll_tt(vector<Mat> image, vector<Mat> &result){
		result.resize(image.size());
		for (int i=0; i < image.size(); ++i){
			result[i] = aisl.normalize(image.at(i));
		}
	}
	void iluminateAll_tt(vector<vector<Mat>> image, vector<vector<Mat>> &result){
		for (int i=0; i < image.size(); ++i){
			vector<Mat> vm;
			for (int j=0; j < image.at(i).size(); ++j){
				Mat il = aisl.normalize(image.at(i).at(j));
				vm.push_back(il);
			}
			result.push_back(vm);
		}
	}
	void iluminateAll_tt(vector<vector<vector<Mat>>> image, vector<vector<vector<Mat>>> &result){
		for (int i=0; i < image.size(); ++i){
			vector<vector<Mat>> ss;
			iluminateAll_tt(image.at(i), ss);
			result.push_back(ss);
		}
	}

	void purgeVariable(vector<Mat> &var){
		for (int i = 0; i < var.size(); i++){
			var.back().release();
			var.pop_back();
		}
		var.clear();
		vector<Mat>().swap(var);
	}
	void purgeVariable(vector<vector<Mat>> &var){
		for (int i = 0; i < var.size(); i++){
			purgeVariable(var.back());
			var.pop_back();
		}
		var.clear();
	}
};
