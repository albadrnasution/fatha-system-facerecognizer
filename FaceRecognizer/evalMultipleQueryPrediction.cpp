
#include "evalMultipleQueryPrediction.h"
#include "helper.h"
#include "BimaIlumi.h"
#include "Timer.h"
#include <time.h>
#include <iomanip>


/**
 * Load images from folder into vector of vector Mat. Each dir inside folderPath represents a person's images.
 * Each dir will be put into the first vector.
 * If ilNorm is set to true, the output of Mat will be pre-processed by illumination normalization.
 * All loaded images is the same size and thus the parameter resizeTo must be given.
 */
vector<vector<Mat>> evalMultipleQueryPrediction::loadImages(string folderPath, bool ilNorm, Size resizeTo){
	vector<vector<Mat>> images;
	read_folder_imagesPerPerson(folderPath, images);
	int totalPerson = (int) images.size();
	resizeImages(images, resizeTo.width, resizeTo.height);

	if (ilNorm){
		BimaIlumi aisl;
		for (int p=0; p < images.size(); ++p)
			for (int i=0; i < images.at(p).size(); ++i){
				images[p][i] = aisl.normalize(images.at(p).at(i));
			}
	}
	return images;
}

/**
 * Load images from folder into vector of vector Mat. Each dir inside folderPath represents a person's images.
 * Each dir will be put into the first vector.
 * If ilNorm is set to true, the output of Mat will be pre-processed by illumination normalization.
 * All loaded images is the same size and thus the parameter resizeTo must be given.
 */
map<int, vector<Mat>> evalMultipleQueryPrediction::loadImagesMap(string folderPath, bool ilNorm, Size resizeTo){
	map<int, vector<Mat>> images;
	read_folder_map(folderPath, images);
	int totalPerson = (int) images.size();
	resizeImages(images, resizeTo.width, resizeTo.height);

	if (ilNorm){
		BimaIlumi aisl;
		for (int p=0; p < images.size(); ++p)
			for (int i=0; i < images.at(p).size(); ++i){
				images[p][i] = aisl.normalize(images.at(p).at(i));
			}
	}
	return images;
}
/**
 * Load images from folder into vector of vector Mat. Each dir inside folderPath represents a person's images.
 * Each dir will be put into the first vector.
 * If ilNorm is set to true, the output of Mat will be pre-processed by illumination normalization.
 * All loaded images is the same size and thus the parameter resizeTo must be given.
 */
map<int, vector<vector<Mat>>> evalMultipleQueryPrediction::loadImagesMapQuery(string folderPath, bool ilNorm, Size resizeTo){
	map<int, vector<vector<Mat>>> images;
	read_folder_mapQuery(folderPath, images);
	int totalPerson = (int) images.size();
	resizeImages(images, resizeTo.width, resizeTo.height);

	if (ilNorm){
		BimaIlumi aisl;
		for (int p=0; p < images.size(); ++p)
			for (int i=0; i < images.at(p).size(); ++i){
				for (int j=0; j < images.at(p).at(i).size(); ++j){
					images[p][i][j] = aisl.normalize(images.at(p).at(i).at(j));
				}
			}
	}
	return images;
}

void evalMultipleQueryPrediction::resizeImages(vector<vector<Mat>>  &images, int im_width, int im_height){
	int nPerson = (int) images.size();
	
	for (int p=0; p < nPerson; ++p){
		for (int i=0; i < images.at(p).size(); ++i){
			Mat face_resized;
			cv::resize(images.at(p).at(i), face_resized, Size(im_width, im_height), 1.0, 1.0, INTER_CUBIC);
			images.at(p)[i] = face_resized;
		}
	}
}

void evalMultipleQueryPrediction::resizeImages(map<int, vector<Mat>>  &images, int im_width, int im_height){
	for (std::map<int, vector<Mat>>::iterator it=images.begin(); it!=images.end(); ++it){
		int label = it->first;
		vector<Mat> list  = it->second;
		for (int i=0; i < list.size(); ++i){
			Mat face_resized;
			cv::resize(list[i], face_resized, Size(im_width, im_height), 1.0, 1.0, INTER_CUBIC);
			list[i] = face_resized;
		}
		it->second = list;
	}
}

void evalMultipleQueryPrediction::resizeImages(map<int, vector<vector<Mat>>>  &images, int im_width, int im_height){
	for (std::map<int, vector<vector<Mat>>>::iterator it=images.begin(); it!=images.end(); ++it){
		int label = it->first;
		vector<vector<Mat>> list  = it->second;
		resizeImages(list, im_width, im_height);
		it->second = list;
	}
}


/**
 * Evaluate multple query prediction with a number of training images, test images, taken from the subset of
 * images within folder with number of ntot. The evaluation will be done randomly in regard to selection of 
 * test and training set. This will be done nexp times.
 */
void evalMultipleQueryPrediction::eval_random(string folder, int ntrain, int ntest, int size, int nexp){
	std::time_t t = std::time(0);
	string db_name = folder;

	std::replace(db_name.begin(), db_name.end(), '/', '.');
	std::replace(db_name.begin(), db_name.end(), '\\', '.');
	//<< "sz_" << size << " " 
	std::stringstream ss; ss << "log/" << SET_PREFIX;
	if (SET_PRINT_PREFSIZE) ss << "sz-" << size;
	ss << db_name << "    tra-" << ntrain << "  tst-" << ntest << " iter-" << nexp << "  t-" << t << ".txt";
	log_name = ss.str();

	Size resizedSize(size, size);
	
	ofstream log; log.open(log_name, ios::app);
	log << "Experiment stdev with TOP ONE PREDICTION" << endl;
	log << "FOLDER : " << folder << endl;
	log << "N TRAINING IMAGES: " << ntrain << endl;
	log << "N IMAGES per QUERY: " << ntest << endl;
	log << "TOTAL ITERATION: " << nexp << endl << endl;
	log << "Resized Size: " << resizedSize << endl << endl;
	log.close();

	if (SET_3PRED){
		ofstream log3; log3.open("3" + log_name, ios::app);
		log3 << "Experiment stdev with TOP THREE PREDICTION, guaranteed to be the same combination with ONE counterpart" << endl;
		log3 << "FOLDER : " << folder << endl;
		log3 << "N TRAINING IMAGES: " << ntrain << endl;
		log3 << "N IMAGES per QUERY: " << ntest << endl;
		log3 << "TOTAL ITERATION: " << nexp << endl << endl;
		log3 << "Resized Size: " << resizedSize << endl << endl;
		log3.close();
	}
	
	// load image without illumination normalization
	std::cout << "Now reading file images as " << resizedSize <<" ... " << endl;
	vector<vector<Mat>> images_ori = loadImages(folder, false, resizedSize);
	eval_random(images_ori, ntrain, ntest, nexp);
}

/**
 * Evaluate multple query prediction with a number of training images, test images, taken from the subset of
 * images within folder with number of ntot. The evaluation will be done randomly in regard to selection of 
 * test and training set. This will be done nexp times.
 */
void evalMultipleQueryPrediction::eval_random(vector<vector<Mat>> images_ori, int ntrain, int ntest, int nexp){
	ofstream log; log.open(log_name, ios::app);
	ofstream log3; 
	if (SET_3PRED) log3.open("3" + log_name, ios::app);
	
	const int ORI_CORRECT = 0, ORI_TEST = 1, ILU_CORRECT = 2, ILU_TEST = 3;

	log << "====================================================================================================" << endl;
	//log << "index_iter, min_dist N_correct,  min_dist N_times , min_dist current_total_accuracy, inv_dist N_correct min_dist, inv_dist N_times , inv_dist current_total_accuracy" << endl;
	
	//int total_correct = 0, total_testing = 0, inv_total_correct = 0, inv_total_testing = 0;
	//int il_total_correct = 0, il_total_testing = 0, il_inv_total_correct = 0, il_inv_total_testing = 0;
	//first one is mode of summation, two is eval variable
	//int tested_mode[14] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
	//const int N_TESTED_MODE = 14;

	int total_eval[cfr::multi_prediction::N_MODE][4];
	int total3_eval[cfr::multi_prediction::N_MODE][4];
	double timer_eval[cfr::multi_prediction::N_MODE], tmilu_eval[cfr::multi_prediction::N_MODE];
	for (int mode = 0; mode < cfr::multi_prediction::N_MODE; ++mode){
		total_eval[mode][0] = 0;
		total_eval[mode][1] = 0;
		total_eval[mode][2] = 0;
		total_eval[mode][3] = 0;
		total3_eval[mode][0] = 0;
		total3_eval[mode][1] = 0;
		total3_eval[mode][2] = 0;
		total3_eval[mode][3] = 0;
		timer_eval[mode] = 0;
		tmilu_eval[mode] = 0;
	}

	vector<double> rateHistory;

	// do experiment as many as nexp
	for (int iex = 0; iex < nexp; ++iex){
		// start of one iteration
		if (iex!=0)  log.open(log_name, ios::app);
		if (iex!=0 && SET_3PRED)  log3.open("3" + log_name, ios::app);
		// message
		std::cout << "Exp " << iex << " | Now ";

		// separate images to training-set and test-set
		vector<vector<Mat>> remainingImage;
		vector<vector<Mat>> trainingSet;
		std::cout << "Separating..." ;
		separate_random(images_ori, ntrain, ntest, trainingSet, remainingImage);

		// streamline image training from vector of vector to vector of images and label
		vector<Mat> image_train, im_ilumi_train;
		std::cout << "\b\b\b\b\b\b\b\b\b\b\b\b\bPreparing... " ;
		vector<int> label_train;
		streamline(trainingSet, image_train, label_train);
		purgeVariable(trainingSet);

		iluminateAll_tt(image_train, im_ilumi_train);

		// train model with training images
		std::cout << "\b\b\b\b\b\b\b\b\b\b\b\b\bTraining...  " ;
		cfr::multi_prediction mult;
		mult.train(image_train, label_train);
		// massacre
		purgeVariable(image_train);

		// training ilumi
		std::cout << "\b\b\b\b\b\b\b\b\b\b\b\b\bTrain-ilu... " ;
		cfr::multi_prediction mult_ilumi;
		mult_ilumi.train(im_ilumi_train, label_train);
		// massacre
		purgeVariable(im_ilumi_train);

		
		//int temp_correct = 0, temp_testing = 0, inv_temp_correct = 0, inv_temp_testing = 0;
		//int il_temp_correct = 0, il_temp_testing = 0, il_inv_temp_correct = 0, il_inv_temp_testing = 0;
		//first one is mode of summation, two is eval variable
		int temps_eval[cfr::multi_prediction::N_MODE][4] ;
		int temps3_eval[cfr::multi_prediction::N_MODE][4] ;
		for (int mode = 0; mode < mult.N_MODE; ++mode){
			temps_eval[mode][0] = 0;
			temps_eval[mode][1] = 0;
			temps_eval[mode][2] = 0;
			temps_eval[mode][3] = 0;
			temps3_eval[mode][0] = 0;
			temps3_eval[mode][1] = 0;
			temps3_eval[mode][2] = 0;
			temps3_eval[mode][3] = 0;
		}

		// test every person (one person as one experiment)
		std::cout << "\b\b\b\b\b\b\b\b\b\b\b\b\bTesting: " ;
		Timer tmr;
		for (int testPerson = 0; testPerson < remainingImage.size(); ++testPerson){
			if (testPerson % 5 == 0) std::cout << testPerson << " ";
			vector<Mat> remi = remainingImage.at(testPerson);
			
			int n_remi = (int) remi.size();
			int ndiff = n_remi - ntest;
			if (ndiff > 0){
			remi.resize(n_remi + ntest);
			std::copy (remi.begin(), remi.begin() + ntest, remi.begin() + n_remi);
			}else{
				n_remi = 0;
			}
			// the sets of test image from this person p
			for (int ii = 0; ii < n_remi; ++ii){
				vector<Mat> cur_query(ntest);
				// copy ntest image as sequence from ii to ii+n_rem
				std::copy (remi.begin() + ii, remi.begin() + ii + ntest, cur_query.begin());
				
				// do eval
				/// ===================================================================== ORI AREA
				//std::cout << "ORI-MIN " ;
				for (int mm = 0; mm < N_TESTED_MODE; ++mm){
					int mode = tested_mode[mm];
					tmr.reset();
					mult.resetQuery();
					mult.mode = mode; 
					mult.addImageQueries(cur_query);
					if (mult.predict()==testPerson){
						total_eval[mode][ORI_CORRECT]++;
						temps_eval[mode][ORI_CORRECT]++;
					}
					timer_eval[mode] += tmr.elapsed();
					total_eval[mode][ORI_TEST]++;
					temps_eval[mode][ORI_TEST]++;
					
					// eval also 3 prediction if required
					if (SET_3PRED){
						if (mult.predictThree(testPerson)){
							total3_eval[mode][ORI_CORRECT]++;
							temps3_eval[mode][ORI_CORRECT]++;
						}
						total3_eval[mode][ORI_TEST]++;
						temps3_eval[mode][ORI_TEST]++;
					}
				}

				/// ===================================================================== ILUM AREA
				// illuminate query and do eval in ilumiNorm
				vector<Mat> ilu_query;
				iluminateAll_tt(cur_query, ilu_query);
				//massacre
				purgeVariable(cur_query);

				//for (int mode = 0; mode < mult.N_MODE; ++mode){
				for (int mm = 0; mm < N_TESTED_MODE; ++mm){

					int mode = tested_mode[mm];
					//std::cout << mult.modeName() ;
					tmr.reset();
					mult_ilumi.resetQuery(); 
					mult_ilumi.mode = mode;
					mult_ilumi.addImageQueries(ilu_query);
					if (mult_ilumi.predict()==testPerson){
						total_eval[mode][ILU_CORRECT]++;
						temps_eval[mode][ILU_CORRECT]++;
					}
					tmilu_eval[mode] += tmr.elapsed();
					total_eval[mode][ILU_TEST]++;
					temps_eval[mode][ILU_TEST]++;

					// eval also 3 prediction if required
					if (SET_3PRED){
						if (mult_ilumi.predictThree(testPerson)){
							total3_eval[mode][ILU_CORRECT]++;
							temps3_eval[mode][ILU_CORRECT]++;
						}
						total3_eval[mode][ILU_TEST]++;
						temps3_eval[mode][ILU_TEST]++;
					}
				}
				/// ===================================================================== END ITER
				
				//massacre
				purgeVariable(ilu_query);
			}
			//massacre
			purgeVariable(remi);
		}
		//massacre
		purgeVariable(remainingImage);

		// message for currently tested training images
		log << iex ;
		for (int mode = 0; mode < cfr::multi_prediction::N_MODE; mode++){
			if ( total_eval[mode][ORI_TEST] != 0){
				log << ", " << "ori-mode = " << mode;
				log << ", " << temps_eval[mode][ORI_CORRECT];
				log << ", " << total_eval[mode][ORI_CORRECT] << ", " << total_eval[mode][ORI_TEST];
				log << ", " << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[mode][ORI_CORRECT] / total_eval[mode][ORI_TEST];
				log << ", " << "ilu-mode = " << mode;
				log << ", " << temps_eval[mode][ILU_CORRECT];
				log << ", " << total_eval[mode][ILU_CORRECT] << ", " << total_eval[mode][ILU_TEST];
				log << ", " << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[mode][ILU_CORRECT] / total_eval[mode][ILU_TEST];
				log << ",\t";
			}
		}
		log << endl;
		log.close();
		std::cout << "\r";
		
		//logging for three prediction
		if (SET_3PRED){
			log3 << iex ;
			for (int mode = 0; mode < cfr::multi_prediction::N_MODE; mode++){
				if ( total_eval[mode][ORI_TEST] != 0){
					log3 << ", " << "ori-mode = " << mode;
					log3 << ", " << temps3_eval[mode][ORI_CORRECT];
					log3 << ", " << total3_eval[mode][ORI_CORRECT] << ", " << total3_eval[mode][ORI_TEST];
					log3 << ", " << std::left << std::setw(7) << std::setprecision(5) << (double) total3_eval[mode][ORI_CORRECT] / total3_eval[mode][ORI_TEST];
					log3 << ", " << "ilu-mode = " << mode;
					log3 << ", " << temps3_eval[mode][ILU_CORRECT];
					log3 << ", " << total3_eval[mode][ILU_CORRECT] << ", " << total3_eval[mode][ILU_TEST];
					log3 << ", " << std::left << std::setw(7) << std::setprecision(5) << (double) total3_eval[mode][ILU_CORRECT] / total3_eval[mode][ILU_TEST];
					log3 << ",\t";
				}
			}
			log3 << endl;
			log3.close();
		}


		// find deviation on the last 25 iteration
		rateHistory.push_back( (double) 100 * total_eval[modeForStdevCheck][ORI_CORRECT] / total_eval[modeForStdevCheck][ORI_TEST]);

		if (iex >= 24 && (iex+1) % 5 == 0){
			// after 25 iterations, check the stdev every 5 iterations for the past 25 iterations
			double sum = std::accumulate(rateHistory.begin(), rateHistory.end(), 0.0);
			double mean = sum / rateHistory.size();
			std::vector<double> diff(rateHistory.size());
			std::transform(rateHistory.begin(), rateHistory.end(), diff.begin(), std::bind2nd(std::minus<double>(), mean));
			double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
			double stdev = std::sqrt(sq_sum / rateHistory.size());

			cout << "                                                                                                   ";
			std::cout << "\r";
			cout << iex << ":  latest=" << std::setprecision(5) << rateHistory.back() << "  mean="<< mean << "  stdev=" << stdev << "  " << endl;
			if (stdev < STDEVBREAK()){
				cout << "Minimum STDEV " << STDEVBREAK() << " is achieved!" << endl;
				break;
			}
			rateHistory.erase(rateHistory.begin(), rateHistory.begin()+5);
		}


	} // end of one iteration


	

	log.open(log_name, ios::app);
	cout << "Recognition rate for top one prediction:" << endl;
	log << "Recognition rate for top one prediction:" << endl;
	for (int mode = 0; mode < cfr::multi_prediction::N_MODE; mode++){
		if ( total_eval[mode][ORI_TEST] != 0){
			String modeName = cfr::multi_prediction::modeName(mode);
			std::cout << modeName << ": ";
			std::cout << "ori-im = ";
			std::cout << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[mode][ORI_CORRECT] / total_eval[mode][ORI_TEST];
			std::cout << ", ";
			std::cout << "ilu-im = ";
			std::cout << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[mode][ILU_CORRECT] / total_eval[mode][ILU_TEST];
			std::cout << endl;

			log << modeName << ": ";
			log << "ori-im = " << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[mode][ORI_CORRECT] / total_eval[mode][ORI_TEST];
			log << ", ";
			log << "ilu-im = " << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[mode][ILU_CORRECT] / total_eval[mode][ILU_TEST];
			log << endl;
		}
	}
	std::cout << endl;
	log << endl;

	if (SET_3PRED){
		log3.open("3"+log_name, ios::app);
		cout << "Recognition rate for TOP THREE prediction:" << endl;
		log3 << "Recognition rate for TOP THREE prediction:" << endl;
		for (int mode = 0; mode < cfr::multi_prediction::N_MODE; mode++){
			if ( total3_eval[mode][ORI_TEST] != 0){
				String modeName = cfr::multi_prediction::modeName(mode);
				std::cout << modeName << ": ";
				std::cout << "ori-im = ";
				std::cout << std::left << std::setw(7) << std::setprecision(5) << (double) total3_eval[mode][ORI_CORRECT] / total3_eval[mode][ORI_TEST];
				std::cout << ", ";
				std::cout << "ilu-im = ";
				std::cout << std::left << std::setw(7) << std::setprecision(5) << (double) total3_eval[mode][ILU_CORRECT] / total3_eval[mode][ILU_TEST];
				std::cout << endl;

				log3 << modeName << ": ";
				log3 << "ori-im = " << std::left << std::setw(7) << std::setprecision(5) << (double) total3_eval[mode][ORI_CORRECT] / total3_eval[mode][ORI_TEST];
				log3 << ", ";
				log3 << "ilu-im = " << std::left << std::setw(7) << std::setprecision(5) << (double) total3_eval[mode][ILU_CORRECT] / total3_eval[mode][ILU_TEST];
				log3 << endl;
			}
		}
		std::cout << endl;
		log3 << endl;
	}

	cout << "Processing time per prediction:" << endl;
	for (int mode = 0; mode < cfr::multi_prediction::N_MODE; mode++){
		if (timer_eval[mode] != 0){
			String modeName = cfr::multi_prediction::modeName(mode);

			std::cout << modeName << ": ";
			std::cout << "ori-time = " << (double)  timer_eval[mode] / total_eval[mode][ORI_TEST] << " ms";
			std::cout << ", ";
			std::cout << "ilu-time = " << (double)  tmilu_eval[mode] / total_eval[mode][ILU_TEST] << " ms";
			std::cout << endl;

			log << modeName << ": ";
			log << "ori-time = " << (double)  timer_eval[mode] / total_eval[mode][ORI_TEST]  << " ms";
			log << ", ";
			log << "ilu-time = " << (double)  tmilu_eval[mode] / total_eval[mode][ILU_TEST] << " ms";
			log << endl;
		}
	}
	log.close();

}

/**
 * Separate images into ngroup of virtually equal number of images.
 * This function assume each person has the same number of images.
 */
void evalMultipleQueryPrediction::separate_random(vector<vector<Mat>> images, int ntrain, int ntest, vector<vector<Mat>> &trainingSet, vector<vector<vector<Mat>>> &testSets){
	
	// iterate within images for each person
	int nPerson = (int) images.size();
	for (int p=0; p < nPerson; ++p){
		vector<Mat> pimag = images.at(p);
		int psize = (int) pimag.size();

		// randomize the order of pimag
		srand((unsigned int)time(0));
		std::random_shuffle(pimag.begin(), pimag.end());

		// extract training image from the front of randomized-pimag 
		vector<Mat> pi_train(ntrain), pi_rest(psize-ntrain);
		std::copy (pimag.begin(), pimag.begin() + ntrain, pi_train.begin());
		trainingSet.push_back(pi_train);

		// copy the rest of vector, from position ntrain to the end
		std::copy (pimag.begin() + ntrain, pimag.end(), pi_rest.begin());
		int n_remimage = (int) pi_rest.size();
		pi_rest.resize(n_remimage * 2);
		std::copy (pi_rest.begin(), pi_rest.begin() + n_remimage, pi_rest.begin() + n_remimage);
		// the sets of test image from this person p
		vector<vector<Mat>> pi_all_testSet;
		for (int ii = 0; ii < n_remimage; ++ii){
			vector<Mat> new_testset(ntest);
			// copy ntest image as sequence from ii to ii+n_rem
			std::copy (pi_rest.begin() + ii, pi_rest.begin() + ii + ntest, new_testset.begin());
			// save to all test set container
			pi_all_testSet.push_back(new_testset);
		}
		// add this person test set to container
		testSets.push_back(pi_all_testSet);
	}
}

/**
 * Separate images into ngroup of virtually equal number of images.
 * This function assume each person has the same number of images.
 */
void evalMultipleQueryPrediction::separate_random(vector<vector<Mat>> images, int ntrain, int ntest, vector<vector<Mat>> &trainingSet, vector<vector<Mat>> &remains){
	
	// iterate within images for each person
	int nPerson = (int) images.size();
	for (int p=0; p < nPerson; ++p){
		vector<Mat> pimag = images.at(p);
		int psize = (int) pimag.size();

		// randomize the order of pimag
		srand((unsigned int)time(0));
		std::random_shuffle(pimag.begin(), pimag.end());

		// extract training image from the front of randomized-pimag 
		vector<Mat> pi_train(ntrain), pi_rest(psize-ntrain);
		std::copy (pimag.begin(), pimag.begin() + ntrain, pi_train.begin());
		trainingSet.push_back(pi_train);

		// copy the rest of vector, from position ntrain to the end
		std::copy (pimag.begin() + ntrain, pimag.end(), pi_rest.begin());
		remains.push_back(pi_rest);
	}
}



/** Separate the dataset into test set and training set
 *  based on a string bitmask that is generated by combinator outside the function
 */
void evalMultipleQueryPrediction::separate_combi(string trainBitmask, vector<vector<Mat>> images, vector<vector<Mat>> &image_test, vector<Mat> &image_train, vector<int> &label_train)
{
	int nPerson = (int)  images.size();
	// iterate every person on dataset
	for (int p=0; p < nPerson; ++p){
		int psize = (int) images.at(p).size();
		vector<Mat> itest;
		// for a person, select image on bitmask=1 as training images
		for (int t = 0; t < trainBitmask.size(); t++){
			Mat img_atp = images.at(p).at(t);
			if (trainBitmask[t]){
				// bimtask true: it means now training image
				// push training image
				image_train.push_back(img_atp);
				label_train.push_back(p);
			}else{
				// select the other as test image (of this person)
				itest.push_back(img_atp);
			}
		}
		// push the container of this person's test set to all test-set container
		image_test.push_back(itest);
	}
}


/**
 * Evaluate multple query prediction with a number of training images, test images, taken from the subset of
 * images within folder with number of ntot. The evaluation will be done randomly in regard to selection of 
 * test and training set. This will be done nexp times.
 */
void evalMultipleQueryPrediction::eval_allcombi(String folder, int nImagePerPerson, int ntrain, int ntest){
	std::time_t t = std::time(0);
	string db_name = folder;
	int size = 64;

	std::replace(db_name.begin(), db_name.end(), '/', '.');
	std::replace(db_name.begin(), db_name.end(), '\\', '.');
	//<< "sz_" << size << " " 
	std::stringstream ss; ss << "log/" << SET_PREFIX;
	if (SET_PRINT_PREFSIZE) ss << "sz-" << size;
	ss << db_name << "    tra-" << ntrain << "  tst-" << ntest << " all-cmb" << "  t-" << t << ".txt";
	log_name = ss.str();

	Size resizedSize(size, size);
	
	ofstream log; log.open(log_name, ios::app);
	log << "Experiment stdev with TOP ONE PREDICTION" << endl;
	log << "FOLDER : " << folder << endl;
	log << "N TRAINING IMAGES: " << ntrain << endl;
	log << "N IMAGES per QUERY: " << ntest << endl;
	log << "TOTAL ITERATION: ALL COMBNATION " << endl << endl;
	log << "Resized Size: " << resizedSize << endl << endl;
	log.close();
	
	// load image without illumination normalization
	std::cout << "Now reading file images as " << resizedSize <<" ... " << endl;
	vector<vector<Mat>> images_ori = loadImages(folder, false, resizedSize);
	eval_allcombi(images_ori, nImagePerPerson, ntrain, ntest);
}



void evalMultipleQueryPrediction::eval_allcombi(vector<vector<Mat>> images_ori, int nImagePerPerson, int ntrain, int ntest){
		
	ofstream log; log.open(log_name, ios::app);
	
	int totalPerson = (int) images_ori.size();
	const int ORI_CORRECT = 0, ORI_TEST = 1, ILU_CORRECT = 2, ILU_TEST = 3;

	log << "====================================================================================================" << endl;

	int total_eval[cfr::multi_prediction::N_MODE][4];
	double timer_eval[cfr::multi_prediction::N_MODE], tmilu_eval[cfr::multi_prediction::N_MODE];
	for (int mode = 0; mode < cfr::multi_prediction::N_MODE; ++mode){
		total_eval[mode][0] = 0;
		total_eval[mode][1] = 0;
		total_eval[mode][2] = 0;
		total_eval[mode][3] = 0;
		timer_eval[mode] = 0;
		tmilu_eval[mode] = 0;
	}
	
	//find all combinatorics
	vector<int> people;
	for (int i = 0; i < nImagePerPerson; ++i) { people.push_back(i+1); }
	Combinatorics coco(people, ntrain);
	
	string bitmask = std::string(ntrain, 1);			// K leading 1's
	bitmask.resize(nImagePerPerson , 0);	// N-K trailing 0's


	int iex = 0;
	// Exhaustive Cross Validation
	do {
		std::stringstream ss;
		cout << endl;
		for (int i = 0; i < nImagePerPerson; ++i) // [0..N-1] integers
		{
			if (bitmask[i]) {cout << i << " "; ss << i << " " ;}
		}
		String trainCombiString = ss.str();

		vector<vector<Mat>> image_test;
		vector<Mat> image_train, im_ilumi_train;
		vector<int> label_train;
		separate_combi(bitmask, images_ori, image_test, image_train, label_train);
		
		cout << "Train-ori " ;
		//train model with training images
		cfr::multi_prediction mult;
		mult.train(image_train, label_train);
		
		iluminateAll_tt(image_train, im_ilumi_train);
		
		// training ilumi
		std::cout << "Train-ilu " ;
		cfr::multi_prediction mult_ilumi;
		mult_ilumi.train(im_ilumi_train, label_train);

		string bitmaskTest = std::string(ntest, 1);			// K leading 1's
		bitmaskTest.resize(nImagePerPerson - ntrain, 0);	// N-K trailing 0's

		// test for every combination of the test image
		cout << "Test combination: " << endl;
		do {
			for (int i = 0; i < nImagePerPerson - ntrain; ++i) // [0..N-1] integers
				if (bitmaskTest[i]) cout << "" << i;
			cout << "  ";

			// test every person (as one experiment)
			for (int testPerson = 0; testPerson < totalPerson; ++testPerson){
				vector<Mat> thisPerson_imageTest = image_test.at(testPerson);
				// popup image_test from the same label
				int label = testPerson;

				// test every mode
				for (int mm = 0; mm < N_TESTED_MODE; ++mm){
					int mode = tested_mode[mm];
					mult.resetQuery();
					mult.mode = mode; 
					// take image training from pool only if it is bitmasked
					for (int b=0; b < bitmaskTest.size(); b++){
						if (bitmaskTest[b]) mult.addImageQuery(thisPerson_imageTest.at(b));					
					}

					if (mult.predict()==label){
						total_eval[mode][ORI_CORRECT]++;
					}
					total_eval[mode][ORI_TEST]++;				
				}

				/// ===================================================================== ILUM AREA
				// illuminate query and do eval in ilumiNorm
				vector<Mat> ilu_query;
				iluminateAll_tt(thisPerson_imageTest, ilu_query);
				//massacre
				purgeVariable(thisPerson_imageTest);

				//for (int mode = 0; mode < mult.N_MODE; ++mode){
				for (int mm = 0; mm < N_TESTED_MODE; ++mm){
					int mode = tested_mode[mm];
					mult_ilumi.resetQuery(); 
					mult_ilumi.mode = mode;
					// take image training from pool only if it is bitmasked
					for (int b=0; b < bitmaskTest.size(); b++){
						if (bitmaskTest[b]) mult_ilumi.addImageQuery(ilu_query.at(b));					
					}
					if (mult_ilumi.predict()==label){
						total_eval[mode][ILU_CORRECT]++;
					}
					total_eval[mode][ILU_TEST]++;
				}
				//massacre
				purgeVariable(ilu_query);
			}
			//end person iteration

		}
		while(std::prev_permutation(bitmaskTest.begin(), bitmaskTest.end()));
		cout << endl;

		// log for this training combination
		
		// message for currently tested training images
		if (iex!=0)  log.open(log_name, ios::app);
		log << trainCombiString ;
		for (int mode = 0; mode < cfr::multi_prediction::N_MODE; mode++){
			if ( total_eval[mode][ORI_TEST] != 0){
				log << ", " << "ori-mode = " << mode;
				log << ", " << total_eval[mode][ORI_CORRECT] << ", " << total_eval[mode][ORI_TEST];
				log << ", " << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[mode][ORI_CORRECT] / total_eval[mode][ORI_TEST];
				log << ", " << "ilu-mode = " << mode;
				log << ", " << total_eval[mode][ILU_CORRECT] << ", " << total_eval[mode][ILU_TEST];
				log << ", " << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[mode][ILU_CORRECT] / total_eval[mode][ILU_TEST];
				log << ",\t";
			}
		}
		log << endl;
		log.close();
		std::cout << endl;
		iex++;
	}
	while(std::prev_permutation(bitmask.begin(), bitmask.end()));


	log.open(log_name, ios::app);
	cout << "Recognition rate for top one prediction:" << endl;
	log << "Recognition rate for top one prediction:" << endl;
	for (int mode = 0; mode < cfr::multi_prediction::N_MODE; mode++){
		if ( total_eval[mode][ORI_TEST] != 0){
			String modeName = cfr::multi_prediction::modeName(mode);
			std::cout << modeName << ": ";
			std::cout << "ori-im = ";
			std::cout << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[mode][ORI_CORRECT] / total_eval[mode][ORI_TEST];
			std::cout << ", ";
			std::cout << "ilu-im = ";
			std::cout << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[mode][ILU_CORRECT] / total_eval[mode][ILU_TEST];
			std::cout << endl;

			log << modeName << ": ";
			log << "ori-im = " << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[mode][ORI_CORRECT] / total_eval[mode][ORI_TEST];
			log << ", ";
			log << "ilu-im = " << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[mode][ILU_CORRECT] / total_eval[mode][ILU_TEST];
			log << endl;
		}
	}
	std::cout << endl;
	log << endl;
}


void evalMultipleQueryPrediction::separate_online(map<int, vector<Mat>> mapList, vector<Mat> &image, vector<int> &label)
{
	for (std::map<int, vector<Mat>>::iterator it=mapList.begin(); it!=mapList.end(); ++it){
		int lbl = it->first;
		vector<Mat> list  = it->second;
		for (Mat mat : list){
			image.push_back(mat);
			label.push_back(lbl);
		}
	}
}


void evalMultipleQueryPrediction::online_generate_queryDelay(vector<Mat> input, int delay,  vector<vector<Mat>> &output)
{
	if (input.size() < delay){
		output.push_back(input);
	}else{
		int max_nq = input.size() / delay;
		for (int i = 0; i < delay; ++i){
			vector<Mat> oneSet;
			for (int j = 0; j < max_nq; ++j){
				int index = j * delay + i;
				if (index > input.size()) break;
				else oneSet.push_back(input[index]);
			}
			output.push_back(oneSet);
		}
	}
}
	
void evalMultipleQueryPrediction::eval_online(string folderTrain, string folderQuery, int DELAY, int SIZE){
	std::time_t t = std::time(0);
	string db_name = folderTrain;
	int size = SIZE;

	std::replace(db_name.begin(), db_name.end(), '/', '.');
	std::replace(db_name.begin(), db_name.end(), '\\', '.');
	//<< "sz_" << size << " " 
	std::stringstream ss; ss << "log/" << SET_PREFIX;
	ss << folderTrain << " "<< folderQuery;
	if (SET_PRINT_PREFSIZE) ss << " sz-" << size;
	ss << "   " << " delay-" << DELAY <<"  t-" << t << ".txt";;
	log_name = ss.str();

	Size resizedSize(size, size);
	
	ofstream log; log.open(log_name, ios::app);
	log << "Experiment stdev with TOP ONE PREDICTION" << endl;
	log << "FOLDER TRAIN : " << folderTrain << endl;
	log << "FOLDER QUERY : " << folderQuery << endl;
	log << "--- ALL IMAGE are USED" << endl << endl;
	log << "Resized Size: " << resizedSize << endl << endl;
	log.close();
	
	// load image without illumination normalization
	std::cout << "Now reading file images as " << resizedSize <<" ... " << endl;
	cout << endl << "Training student " ;
	map<int, vector<Mat>> traines_ori = loadImagesMap(folderTrain, false, resizedSize);
	cout << endl << "Reading query " ;
	map<int, vector<vector<Mat>>> queries_ori = loadImagesMapQuery(folderQuery, false, resizedSize);
	cout << endl << "Starting eval" << endl;
	eval_online(traines_ori , queries_ori, DELAY);
}

void evalMultipleQueryPrediction::eval_online(map<int, vector<Mat>> trains_ori, map<int, vector<vector<Mat>>> queries_ori, int DELAY){
	const int RANK_TEST = 10;

	ofstream log; log.open(log_name, ios::app);
	
	const int ORI_CORRECT = 0, ORI_TEST = 1, ILU_CORRECT = 2, ILU_TEST = 3;
	log << "====================================================================================================" << endl;
	int total_eval[RANK_TEST][cfr::multi_prediction::N_MODE][4];
	for (int rank = 0; rank < RANK_TEST; ++rank){
		for (int mode = 0; mode < cfr::multi_prediction::N_MODE; ++mode){
			total_eval[rank][mode][0] = 0; // ORI_CORRECT
			total_eval[rank][mode][1] = 0; // ORI_TEST
			total_eval[rank][mode][2] = 0; // ILU_CORRECT
			total_eval[rank][mode][3] = 0; // ILU_TEST
		}
	}

	vector<Mat> trains_ori_imgs;
	vector<int> trains_lbls;
	separate_online(trains_ori, trains_ori_imgs, trains_lbls);
	
	cout << "Train ORI " << trains_ori_imgs.size() << " " << trains_lbls.size() << endl;
	cfr::multi_prediction mult;
	mult.train(trains_ori_imgs, trains_lbls);
	
	cout << "Iluminate Training Image " << endl;
	vector<Mat> trains_ilu_imgs;
	iluminateAll_tt(trains_ori_imgs, trains_ilu_imgs);
	
	cout << "Train ILU " << trains_ilu_imgs.size() << endl;
	cfr::multi_prediction mult_ilu;
	mult_ilu.train(trains_ilu_imgs, trains_lbls);

	
	vector<int> nquery[cfr::multi_prediction::N_MODE], nquery_ilu[cfr::multi_prediction::N_MODE];
	
	cout << "Starting Test" << endl;
	// Let's test!
	for (std::map<int, vector< vector<Mat> >>::iterator it=queries_ori.begin(); it!=queries_ori.end(); ++it){
		int testPerson = it->first;
		cout << "Testing " << testPerson <<", ";
		vector<vector<Mat>> allSequence = it->second;
		
		for (vector<Mat> oneSequence : allSequence){
			vector<Mat> allPossibleQuery = oneSequence;
			vector<vector<Mat>> generatedQueries; 
			online_generate_queryDelay(allPossibleQuery, DELAY, generatedQueries);

			for (vector<Mat> sequenceQuery : generatedQueries){

				//if (sequenceQuery.size() >= 5) {
					std::cout << "O" ;
					for (int mm = 0; mm < N_TESTED_MODE; ++mm){
						int mode = tested_mode[mm];
						mult.resetQuery();
						mult.mode = mode; 
						//TODO: set query one by one and check result
						//See the vote/nq
						mult.addImageQueries(sequenceQuery);

						if (mult.getNQuery() >=5 ){
							vector<int> rankedResult = mult.returnRanked(10);
							bool found = false;
							for (int r = 0; r < RANK_TEST; r++){
								if (rankedResult[r] == testPerson) found = true;
								if (found) total_eval[r][mode][ORI_CORRECT]++;
								total_eval[r][mode][ORI_TEST]++;
							}

							nquery[mode].push_back(mult.getNQuery());
						}
					}

					vector<Mat> sequenceQueryIlumi;
					iluminateAll_tt(sequenceQuery, sequenceQueryIlumi);
		
					std::cout << "I" ;
					for (int mm = 0; mm < N_TESTED_MODE; ++mm){
						int mode = tested_mode[mm];
						mult_ilu.resetQuery();
						mult_ilu.mode = mode; 
						mult_ilu.addImageQueries(sequenceQueryIlumi);
						
						if (mult_ilu.getNQuery() >=5 ){
							vector<int> rankedResult = mult_ilu.returnRanked(10);
							bool found = false;
							for (int r = 0; r < RANK_TEST; r++){
								if (rankedResult[r] == testPerson) found = true;
								if (found) total_eval[r][mode][ILU_CORRECT]++;
								total_eval[r][mode][ILU_TEST]++;
							}
							nquery_ilu[mode].push_back(mult_ilu.getNQuery());
						}
					}
				//}//END seqQUery Size
			}// END seqQuery
		}//END oneSeq
		std::cout << endl;

		std::cout << "Temp ORIGINAL IMAGE = " << endl;
		for (int mode = 0; mode < cfr::multi_prediction::N_MODE; mode++){
			if ( total_eval[0][mode][ORI_TEST] != 0){
				String modeName = cfr::multi_prediction::modeName(mode);
				std::cout << modeName << ": ,  ";
				for (int rank = 0; rank < RANK_TEST; ++rank){
					std::cout << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[rank][mode][ORI_CORRECT] / total_eval[rank][mode][ORI_TEST];
					std::cout << ", ";
			
				}
				std::cout << "      " << total_eval[9][mode][ORI_CORRECT] << " " << total_eval[9][mode][ORI_TEST] << endl;
			}
		}


		std::cout << "Temp ILUMI IMAGE = " << endl;
		for (int mode = 0; mode < cfr::multi_prediction::N_MODE; mode++){
			if ( total_eval[0][mode][ILU_TEST] != 0){
				String modeName = cfr::multi_prediction::modeName(mode);
				std::cout << modeName << ": ,  ";
				for (int rank = 0; rank < RANK_TEST; ++rank){
					std::cout << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[rank][mode][ILU_CORRECT] / total_eval[rank][mode][ILU_TEST];
					std::cout << ", ";	
				}
				std::cout << "      " << total_eval[9][mode][ILU_CORRECT] << " " << total_eval[9][mode][ILU_TEST] << endl;
			}
		}
		std::cout << endl;

	}// end ITERATION (people)

	
	std::cout << endl << endl << endl << "Resulf of ORIGINAL IMAGE = " << endl;
	log << "ORIGINAL IMAGE = " << endl;
	for (int mode = 0; mode < cfr::multi_prediction::N_MODE; mode++){
		if ( total_eval[0][mode][ORI_TEST] != 0){
			String modeName = cfr::multi_prediction::modeName(mode);
			std::cout << modeName << ": ,  ";
			log		  << modeName << ": ,  ";
			for (int rank = 0; rank < RANK_TEST; ++rank){
				std::cout << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[rank][mode][ORI_CORRECT] / total_eval[rank][mode][ORI_TEST];
				std::cout << ", ";

				log << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[rank][mode][ORI_CORRECT] / total_eval[rank][mode][ORI_TEST];
				log << ", ";			
			}
			std::cout << endl;
			log << endl;
		}
	}
	log << endl << "Number of CORRECT: " << endl;
	for (int mode = 0; mode < cfr::multi_prediction::N_MODE; mode++){
		if ( total_eval[0][mode][ORI_TEST] != 0){
			String modeName = cfr::multi_prediction::modeName(mode);
			log		  << modeName << ": ,  ";
			for (int rank = 0; rank < RANK_TEST; ++rank){
				log << std::left << std::setw(4) <<  total_eval[rank][mode][ORI_CORRECT];
				log << ", ";			
			}
			log << "   total_test=" << total_eval[0][mode][ORI_TEST] <<endl;
			log << endl;
		}
	}


	std::cout << endl<< endl << "ILUMI IMAGE = " << endl;
	log       << endl<< endl << "ILUMI IMAGE = " << endl;
	for (int mode = 0; mode < cfr::multi_prediction::N_MODE; mode++){
		if ( total_eval[0][mode][ILU_TEST] != 0){
			String modeName = cfr::multi_prediction::modeName(mode);
			std::cout << modeName << ": ,  ";
			log		  << modeName << ": ,  ";
			for (int rank = 0; rank < RANK_TEST; ++rank){
				std::cout << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[rank][mode][ILU_CORRECT] / total_eval[rank][mode][ILU_TEST];
				std::cout << ", ";
				log << std::left << std::setw(7) << std::setprecision(5) << (double) total_eval[rank][mode][ILU_CORRECT] / total_eval[rank][mode][ILU_TEST];
				log << ", ";			
			}
			std::cout << endl;
			log << endl;
		}
	}
	log << endl << "Number of CORRECT: " << endl;
	for (int mode = 0; mode < cfr::multi_prediction::N_MODE; mode++){
		if ( total_eval[0][mode][ILU_TEST] != 0){
			String modeName = cfr::multi_prediction::modeName(mode);
			log		  << modeName << ": ,  ";
			for (int rank = 0; rank < RANK_TEST; ++rank){
				log << std::left << std::setw(4) <<  total_eval[rank][mode][ILU_CORRECT];
				log << ", ";			
			}
			log << "   total_test=" << total_eval[0][mode][ILU_TEST] <<endl;
			log << endl;
		}
	}
	log << endl;
	
	std::cout << endl << "AVERAGE NUMBER OF UERY = " << endl;
	log << endl << "AVERAGE NUMBER OF UERY = " << endl;
	for (int mode = 0; mode < cfr::multi_prediction::N_MODE; mode++){
		if ( total_eval[0][mode][ILU_TEST] != 0){
			String modeName = cfr::multi_prediction::modeName(mode);
			std::cout << modeName << ": ,  ";
			double sum_ori = std::accumulate( nquery[mode].begin(),  nquery[mode].end(), 0.0);
			double avg_ori = sum_ori /  nquery[mode].size();
			double sum_ilu = std::accumulate( nquery_ilu[mode].begin(),  nquery_ilu[mode].end(), 0.0);
			double avg_ilu = sum_ilu /  nquery_ilu[mode].size();
			std::cout << " ori=" << avg_ori << " ilu=" << avg_ilu << endl;
			log << " ori=" << avg_ori << " ilu=" << avg_ilu << endl;
		}
	}

	log.close();
}