
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/internal.hpp"

#include "facerec_custom.h"
#include "fc/spatial.hpp"
#include "fc/helper.hpp"

#include <iostream>

using std::set;
using namespace libfacerec;

// Define the CV_INIT_ALGORITHM macro for OpenCV 2.4.0:
#ifndef CV_INIT_ALGORITHM
#define CV_INIT_ALGORITHM(classname, algname, memberinit) \
	static Algorithm* create##classname() \
{ \
	return new classname; \
} \
	\
	static AlgorithmInfo& classname##_info() \
{ \
	static AlgorithmInfo classname##_info_var(algname, create##classname); \
	return classname##_info_var; \
} \
	\
	static AlgorithmInfo& classname##_info_auto = classname##_info(); \
	\
	AlgorithmInfo* classname::info() const \
{ \
	static volatile bool initialized = false; \
	\
	if( !initialized ) \
{ \
	initialized = true; \
	classname obj; \
	memberinit; \
} \
	return &classname##_info(); \
}
#endif

namespace customfacerec
{


	class CustomFisherface: public CustomFaceRecognizer
	{
	private:
		int _num_components;
		double _threshold;
		Mat _eigenvectors;
		Mat _eigenvalues;
		Mat _mean;
		vector<Mat> _projections;
		Mat _labels;
		//map<int, Mat> _class_centroid;
		//map<int, int> _count;
		//map<int, Mat> _class_mean;
		//map<int, Mat> _class_icovar;
		int _num_class;

	public:
		using CustomFaceRecognizer::save;
		using CustomFaceRecognizer::load;

		// Initializes an empty Fisherfaces model.
		CustomFisherface(int num_components = 0, double threshold = DBL_MAX) :
			_num_components(num_components),
			_threshold(threshold) {}

		// Initializes and computes a Fisherfaces model with images in src and
		// corresponding labels in labels. num_components will be kept for
		// classification.
		CustomFisherface(InputArray src, InputArray labels,
			int num_components = 0, double threshold = DBL_MAX) :
		_num_components(num_components),
			_threshold(threshold) {
				train(src, labels);
		}

		~CustomFisherface() {
			for (int p = 0 ; p < _projections.size(); ++p){
				_projections.back().release();
				_projections.pop_back();
			}
			_projections.clear();
		}

		// Computes a Fisherfaces model with images in src and corresponding labels
		// in labels.
		void train(InputArray src, InputArray labels);

		// Predicts the label of a query image in src.
		int predict(InputArray src) const;

		// Predicts the label and confidence for a given sample.
		void predict(InputArray _src, int &label, double &dist) const;

		// Predicts the label of a query image in src.
		int predictWithMethod(int method, InputArray src) const;

		// Predicts the label and confidence for a given sample.
		void predictWithMethod(int method, InputArray _src, int &label, double &dist) const;

		// Compute the distance of query to all class
		void computeDistances(InputArray _src, int &label, vector<double> &dist) const;

		// Compute the distance of query to 10 nearest neighbors
		void compute10NN(InputArray src, int &label, multimap<double, int> &dist) const;
		
		// Compute the distance of query to all subspace image available
		void allDistances(InputArray src, map<int, vector<double>> &dist) const;

		// See FaceRecognizer::load.
		virtual void load(const FileStorage& fs);

		// See FaceRecognizer::save.
		virtual void save(FileStorage& fs) const;

		AlgorithmInfo* info() const;
	};


	//------------------------------------------------------------------------------
	// FaceRecognizer
	//------------------------------------------------------------------------------
	void CustomFaceRecognizer::update(InputArrayOfArrays, InputArray) {
		string error_msg = format("This FaceRecognizer (%s) does not support updating, you have to use FaceRecognizer::train to update it.", this->name().c_str());
		CV_Error(CV_StsNotImplemented, error_msg);
	}

	void CustomFaceRecognizer::save(const string& filename) const {
		FileStorage fs(filename, FileStorage::WRITE);
		if (!fs.isOpened())
			CV_Error(CV_StsError, "File can't be opened for writing!");
		this->save(fs);
		fs.release();
	}

	void CustomFaceRecognizer::load(const string& filename) {
		FileStorage fs(filename, FileStorage::READ);
		if (!fs.isOpened())
			CV_Error(CV_StsError, "File can't be opened for writing!");
		this->load(fs);
		fs.release();
	}


	// See FaceRecognizer::load.
	void CustomFisherface::load(const FileStorage& fs) {
		//read matrices
		fs["num_components"] >> _num_components;
		fs["mean"] >> _mean;
		fs["eigenvalues"] >> _eigenvalues;
		fs["eigenvectors"] >> _eigenvectors;
		// read sequences
		libfacerec::readFileNodeList(fs["projections"], _projections);
		fs["labels"] >> _labels;
	}

	// See FaceRecognizer::save.
	void CustomFisherface::save(FileStorage& fs) const {
		// write matrices
		fs << "num_components" << _num_components;
		fs << "mean" << _mean;
		fs << "eigenvalues" << _eigenvalues;
		fs << "eigenvectors" << _eigenvectors;
		// write sequences
		libfacerec::writeFileNodeList(fs, "projections", _projections);
		fs << "labels" << _labels;
	}

	void CustomFisherface::train(InputArray src, InputArray _lbls) {
		if(src.total() == 0) {
			string error_message = format("Empty training data was given. You'll need more than one sample to learn a model.");
			CV_Error(CV_StsBadArg, error_message);
		} else if(_lbls.getMat().type() != CV_32SC1) {
			string error_message = format("Labels must be given as integer (CV_32SC1). Expected %d, but was %d.", CV_32SC1, _lbls.type());
			CV_Error(CV_StsBadArg, error_message);
		}
		// make sure data has correct size
		if(src.total() > 1) {
			for(int i = 1; i < static_cast<int>(src.total()); i++) {
				if(src.getMat(i-1).total() != src.getMat(i).total()) {
					string error_message = format("In the Fisherfaces method all input samples (training images) must be of equal size! Expected %d pixels, but was %d pixels.", src.getMat(i-1).total(), src.getMat(i).total());
					CV_Error(CV_StsUnsupportedFormat, error_message);
				}
			}
		}
		// get data
		Mat labels = _lbls.getMat();
		Mat data = libfacerec::asRowMatrix(src, CV_64FC1);
		// number of samples
		int N = data.rows;
		// make sure labels are passed in correct shape
		if(labels.total() != (size_t) N) {
			string error_message = format("The number of samples (src) must equal the number of labels (labels)! len(src)=%d, len(labels)=%d.", N, labels.total());
			CV_Error(CV_StsBadArg, error_message);
		} else if(labels.rows != 1 && labels.cols != 1) {
			string error_message = format("Expected the labels in a matrix with one row or column! Given dimensions are rows=%s, cols=%d.", labels.rows, labels.cols);
			CV_Error(CV_StsBadArg, error_message);
		}
		// clear existing model data
		_labels.release();
		_projections.clear();
		// safely copy from cv::Mat to std::vector
		vector<int> ll;
		for(int i = 0; i < labels.total(); i++) {
			ll.push_back(labels.at<int>(i));
		}
		// get the number of unique classes
		int C = (int) remove_dups(ll).size();
		// clip number of components to be a valid number
		if((_num_components <= 0) || (_num_components > (C-1)))
			_num_components = (C-1);
		// perform a PCA and keep (N-C) components
		PCA pca(data, Mat(), CV_PCA_DATA_AS_ROW, (N-C));

		// project the data and perform a LDA on it
		LDA lda(pca.project(data),labels, _num_components);
		// store the total mean vector
		_mean = pca.mean.reshape(1,1);
		// store labels
		labels.copyTo(_labels);
		// store the eigenvalues of the discriminants
		lda.eigenvalues().convertTo(_eigenvalues, CV_64FC1);
		// Now calculate the projection matrix as pca.eigenvectors * lda.eigenvectors.
		// Note: OpenCV stores the eigenvectors by row, so we need to transpose it!
		gemm(pca.eigenvectors, lda.eigenvectors(), 1.0, Mat(), 0.0, _eigenvectors, GEMM_1_T);
		// store the projections of the original data
		for(int sampleIdx = 0; sampleIdx < data.rows; sampleIdx++) {
			Mat p = subspaceProject(_eigenvectors, _mean, data.row(sampleIdx));
			_projections.push_back(p);
		}

		// kill all unused
		data.release();
		labels.release();

		////save C as number of unique class
		//_num_class = C;
		//map<int, Mat> _sum;
		//vector <vector<Mat>> proj_per_label;

		//for (int l = 0; l < C; l++){
		//	_count[l] = 0;
		//}
		//// compute an average of each class
		//for(int sampleIdx = 0; sampleIdx < data.rows; sampleIdx++) {
		//	Mat p = subspaceProject(_eigenvectors, _mean, data.row(sampleIdx));
		//	int lb = _labels.at<int> ((int)sampleIdx);
		//	if (_count[lb] == 0){
		//		_sum[lb] = p;
		//		vector<Mat> lbm;
		//		lbm.push_back(p);
		//		proj_per_label.push_back(lbm);
		//	}else{
		//		_sum[lb] = _sum[lb] + p;
		//		proj_per_label.at(lb).push_back(p);
		//	}
		//	_count[lb] += 1;
		//}

		//for (int lb = 0; lb < proj_per_label.size(); lb++){
		//	//compute inverse matrix covariance
		//	Mat covar, mean, icovar;
		//	calcCovarMatrix(proj_per_label.at(lb), covar, mean, CV_COVAR_NORMAL);
		//	invert(covar, icovar, DECOMP_SVD);
		//	_class_mean.insert(pair<int, Mat>(lb, mean));
		//	_class_icovar.insert(pair<int, Mat>(lb, icovar));
		//}
		//// compute class centroid
		//for (int l = 0; l < C; l++){
		//	_class_centroid[l] = _sum[l] / _count[l];
		//}
	}

	void CustomFisherface::predict(InputArray _src, int &minClass, double &minDist) const {
		Mat src = _src.getMat();
		// check data alignment just for clearer exception messages
		if(_projections.empty()) {
			// throw error if no data (or simply return -1?)
			string error_message = "This Fisherfaces model is not computed yet. Did you call Fisherfaces::train?";
			CV_Error(CV_StsBadArg, error_message);
		} else if(src.total() != (size_t) _eigenvectors.rows) {
			string error_message = format("Wrong input image size. Reason: Training and Test images must be of equal size! Expected an image with %d elements, but got %d.", _eigenvectors.rows, src.total());
			CV_Error(CV_StsBadArg, error_message);
		}
		// project into LDA subspace
		Mat q = subspaceProject(_eigenvectors, _mean, src.reshape(1,1));
		// find 1-nearest neighbor
		minDist = DBL_MAX;
		minClass = -1;
		for(size_t sampleIdx = 0; sampleIdx < _projections.size(); sampleIdx++) {
			double dist = norm(_projections[sampleIdx], q, NORM_L2);
			if((dist < minDist) && (dist < _threshold)) {
				minDist = dist;
				minClass = _labels.at<int>((int)sampleIdx);
			}
		}
	}

	void CustomFisherface::predictWithMethod(int method, InputArray _src, int &minClass, double &minDist) const {
		Mat src = _src.getMat();
		// check data alignment just for clearer exception messages
		if(_projections.empty()) {
			// throw error if no data (or simply return -1?)
			string error_message = "This Fisherfaces model is not computed yet. Did you call Fisherfaces::train?";
			CV_Error(CV_StsBadArg, error_message);
		} else if(src.total() != (size_t) _eigenvectors.rows) {
			string error_message = format("Wrong input image size. Reason: Training and Test images must be of equal size! Expected an image with %d elements, but got %d.", _eigenvectors.rows, src.total());
			CV_Error(CV_StsBadArg, error_message);
		}
		// project into LDA subspace
		Mat q = subspaceProject(_eigenvectors, _mean, src.reshape(1,1));
		// find 1-nearest neighbor
		minDist = DBL_MAX;
		minClass = -1;

		if (method == K1NN_DISTANCE){
			//Compute to one nearest image point, and see the label
			for(size_t sampleIdx = 0; sampleIdx < _projections.size(); sampleIdx++) {
				double dist = norm(_projections[sampleIdx], q, NORM_L2);
				if((dist < minDist) && (dist < _threshold)) {
					minDist = dist;
					minClass = _labels.at<int>((int)sampleIdx);
				}
			}
		}
		else if (method == CLASS_CENTROID){
			//Using the saved class centroid (average of subspace image in each class)
			//find nearest centroid
			//TODO: Mahalonobis distance

			//for (int l = 0; l < _count.size(); l++){
			//	//Mat s = _class_centroid.at(l);
			//	//double dist = norm(s, q, NORM_L2);
			//	double dist = Mahalanobis(q, _class_mean.at(l), _class_icovar.at(l));
			//	if((dist < minDist) && (dist < _threshold)) {
			//		minDist = dist;
			//		minClass = l;
			//	}
			//}
		}
		else if (method == CLASS_AVG_DISTANCE){
			//Compute distance to all point, group for every label in training set (ie. every class)
			//Average the distance and return the class with smallest distance
		}


	}

	// Compute the distance of query to all class
	void CustomFisherface::computeDistances(InputArray _src, int &minClass, vector<double> &distAll) const {

		Mat src = _src.getMat();
		// check data alignment just for clearer exception messages
		if(_projections.empty()) {
			// throw error if no data (or simply return -1?)
			string error_message = "This Fisherfaces model is not computed yet. Did you call Fisherfaces::train?";
			CV_Error(CV_StsBadArg, error_message);
		} else if(src.total() != (size_t) _eigenvectors.rows) {
			string error_message = format("Wrong input image size. Reason: Training and Test images must be of equal size! Expected an image with %d elements, but got %d.", _eigenvectors.rows, src.total());
			CV_Error(CV_StsBadArg, error_message);
		}
		// project into LDA subspace
		Mat q = subspaceProject(_eigenvectors, _mean, src.reshape(1,1));

		distAll.empty();
		double minDist = DBL_MAX;
		minClass = -1;
		// find 1-nearest neighbor
		// save the distance for each class
		vector<double> sumClassDistance;
		vector<int>  countClassDistance;
		vector<double>  minClassDistance;

		for (int i = 0; i < _num_class; i++){
			sumClassDistance.push_back(0);
			countClassDistance.push_back(0);
			minClassDistance.push_back(DBL_MAX);
		}

		for(size_t sampleIdx = 0; sampleIdx < _projections.size(); sampleIdx++) {
			double dist = norm(_projections[sampleIdx], q, NORM_L2);
			int l = _labels.at<int>((int)sampleIdx);

			// make summation of distance per class
			sumClassDistance[l] += dist;
			countClassDistance[l] += 1;
			if (dist < minClassDistance[l])
				minClassDistance[l] = dist;

			//distAll.push_back(dist); //dist for each image subspace
			if((dist < minDist) && (dist < _threshold)) {
				minDist = dist;
				minClass = _labels.at<int>((int)sampleIdx);
			}
		}

		distAll = minClassDistance;
		//double minAvgDist = DBL_MAX;
		//int minAvgClass = -1;
		//for (int i = 0; i < _num_class; i++){
		//	double avgDist = sumClassDistance[i] / countClassDistance[i];
		//	distAll.push_back(avgDist);

		//	if((avgDist < minAvgDist) && (avgDist < _threshold)) {
		//		minAvgDist = avgDist;
		//		minAvgClass = i;
		//	}
		//}
		//minClass = minAvgClass;

	}

	
	// Compute the distance of a query every subspace image within the training set
	// The function will return the top 10 smallest distances (10 nearest neighbor) only
	// dist10NN will include <distance, label> pair sorted ascending by the distance
	void CustomFisherface::compute10NN(InputArray _src, int &minClass, multimap<double, int> &dist10NN) const {
		Mat src = _src.getMat();
		// check data alignment just for clearer exception messages
		if(_projections.empty()) {
			// throw error if no data (or simply return -1?)
			string error_message = "This Fisherfaces model is not computed yet. Did you call Fisherfaces::train?";
			CV_Error(CV_StsBadArg, error_message);
		} else if(src.total() != (size_t) _eigenvectors.rows) {
			string error_message = format("Wrong input image size. Reason: Training and Test images must be of equal size! Expected an image with %d elements, but got %d.", _eigenvectors.rows, src.total());
			CV_Error(CV_StsBadArg, error_message);
		}
		// project into LDA subspace
		Mat q = subspaceProject(_eigenvectors, _mean, src.reshape(1,1));

		dist10NN.empty();
		double minDist = DBL_MAX;
		minClass = -1;
		// find 1-nearest neighbor
		// save the distance for each class
		for(size_t sampleIdx = 0; sampleIdx < _projections.size(); sampleIdx++) {
			double dist = norm(_projections[sampleIdx], q, NORM_L2);
			int l = _labels.at<int>((int)sampleIdx);
			
			// insert distance to hashmap as a key
			// so that it will be sorted automatically

			// TODO: insert only 10 items, check if the largest distance in the 10 items is larger than new guys, delete it and insert new guys
			// Kalau bisa ga pake map
			dist10NN.insert(pair<double, int>(dist, l));

			//dist for each image subspace
			if((dist < minDist) && (dist < _threshold)) {
				minDist = dist;
				minClass = _labels.at<int>((int)sampleIdx);
			}
		}

	}

	
	void CustomFisherface::allDistances(InputArray _src, map<int, vector<double>> &all_distances) const {
		Mat src = _src.getMat();
		// check data alignment just for clearer exception messages
		if(_projections.empty()) {
			// throw error if no data (or simply return -1?)
			string error_message = "This Fisherfaces model is not computed yet. Did you call Fisherfaces::train?";
			CV_Error(CV_StsBadArg, error_message);
		} else if(src.total() != (size_t) _eigenvectors.rows) {
			string error_message = format("Wrong input image size. Reason: Training and Test images must be of equal size! Expected an image with %d elements, but got %d.", _eigenvectors.rows, src.total());
			CV_Error(CV_StsBadArg, error_message);
		}
		// project into LDA subspace
		Mat q = subspaceProject(_eigenvectors, _mean, src.reshape(1,1));
		// compute distance, add to container if less than threshold
		for(size_t sampleIdx = 0; sampleIdx < _projections.size(); sampleIdx++) {
			double dist = norm(_projections[sampleIdx], q, NORM_L2);
			if(dist < _threshold) {
				int label = _labels.at<int>((int)sampleIdx);
				map<int, vector<double>>::iterator iter = all_distances.find(label);
				if (iter != all_distances.end()){
					//key label exist, add dist to it
					iter->second.push_back(dist);
				}else{
					vector<double> vd;
					vd.push_back(dist);
					all_distances.insert(pair<int, vector<double>>(label, vd));
					//cout << "::" << label<< endl;
				}
				//cout << "| " << " " << dist;
			}
		}
		q.release();
		src.release();
	}

	int CustomFisherface::predict(InputArray _src) const {
		int label;
		double dummy;
		predict(_src, label, dummy);
		return label;
	}

	int CustomFisherface::predictWithMethod(int method, InputArray _src) const {
		int label;
		double dummy;
		predictWithMethod(method, _src, label, dummy);
		return label;
	}

	Ptr<CustomFaceRecognizer> createCustomFisherFaceRecognizer(int num_components, double threshold)
	{
		return new CustomFisherface(num_components, threshold);
	}


	CV_INIT_ALGORITHM(CustomFisherface, "FaceRecognizer.CustomFisherface",
		obj.info()->addParam(obj, "ncomponents", obj._num_components);
	obj.info()->addParam(obj, "threshold", obj._threshold);
	obj.info()->addParam(obj, "projections", obj._projections, true);
	obj.info()->addParam(obj, "labels", obj._labels, true);
	obj.info()->addParam(obj, "eigenvectors", obj._eigenvectors, true);
	obj.info()->addParam(obj, "eigenvalues", obj._eigenvalues, true);
	obj.info()->addParam(obj, "mean", obj._mean, true));








}

