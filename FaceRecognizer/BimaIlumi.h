#ifndef BIMAILUMI_H
#define BIMAILUMI_H

#include "stdafx.h"
#include <array>


using namespace std;
using namespace cv;

class BimaIlumi {
	static const int wndwSize = 12;
	static const int halfSize = 6;

	//Fuzzy rule of YaleB data training
	std::array<float,25> fuzzyRule;

	int width, height, step, channels;
	Mat imgSource;
	Mat imgNormalized;

	struct histeq
	{
		int p[50000];
	};

	struct histeq hist,subblock;

public:
	BimaIlumi();
	~BimaIlumi(){
		imgSource.release();
		imgNormalized.release();
	}
	BimaIlumi(std::array<float,25>);
	void setRule();
	Mat normalize (Mat);
	int area() {return width*height;}

private:
	void HistEq(int);
	void initSourceImage(Mat);
	float fuzzy_a(float,float);
	float triangle(float,float,float,float);
};


#endif
