
#ifndef COMBINATORICS_H
#define COMBINATORICS_H



#include <vector>
#include <algorithm>
#include <iostream>
#include <string>
 
class Combinatorics{
	
	std::vector<int> _pool;
	std::vector<int> _last_combination;
	std::string _bitmask;
	int _r;
	int _count;
	
public:
	Combinatorics (std::vector<int> pool, int r){
		_pool = pool;
		_r = r;
		_count = 0;

		_bitmask= std::string(_r, 1);			// K leading 1's
		_bitmask.resize(_pool.size(), 0);	// N-K trailing 0's
	}

	void pretty_print(const std::vector<int>& v) {
	  static int count = 0;
	  std::cout << "combination no " << (++count) << ": [ ";
	  for (int i = 0; i < v.size(); ++i) { std::cout << v[i] << " "; }
	  std::cout << "] " << std::endl;
	}

	std::vector<int> next_combination(){
		std::vector<int> combination;

		bool has_more = _count == -1 ? false : _count == 0 ? true : std::prev_permutation(_bitmask.begin(), _bitmask.end());

		if (has_more){
			for (int i = 0; i < _pool.size(); ++i) // [0..N-1] integers
			{
				if (_bitmask[i]) combination.push_back(_pool.at(i));
			}
			_count++;
		}else{
			_count = -1;
		}

		return combination;
	}
};

#endif
