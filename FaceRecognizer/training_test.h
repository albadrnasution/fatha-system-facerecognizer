

#include "stdafx.h"
#include <vector>


using namespace std;
using namespace cv;

void randomlySeparate(int trainNumber, vector<vector<Mat>> images);

double onlineValidationWithAlg(int algorithm, 
	vector<Mat> image_test,
	vector<Mat> image_train,
	vector<int> label_test,
	vector<int> label_train);

double onlineValidationWithAlg2(int algorithm, 
	vector<Mat> image_test,
	vector<Mat> image_train,
	vector<int> label_test,
	vector<int> label_train);


void numeralCrossValOnline(int trainNumber, string folderPath, int nExperiment);

void multipleQueryTest(int trainNumber, string folderPath, int nExperiment);
void multipleQueryEvaluate(int trainNumber, string folderPath, int nExperiment);
void multipleQueryEvaluate_stairs(int trainNumber, int testNumber, int nImagePerPerson, string folderPath);

void multipleQueryEvaluate2(string folderPath, int trainNumber, int testNumber, int nExperiment);

void randomlySeparate_checkPrev(int trainNumber, int testNumber,
	vector<vector<Mat>> images,
	vector<vector<Mat>> &image_test,
	vector<Mat> &image_train,
	vector<int> &label_train);
