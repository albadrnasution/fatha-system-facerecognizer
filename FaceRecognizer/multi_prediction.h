

#include "facerec_custom.h"
#include <numeric>
#include <functional>

namespace customfacerec
{

	class multi_prediction{

		//iterator type
		typedef int studentid;
		typedef map<studentid, vector<double>>::iterator it_mapv;

		vector<int> labelList;
		//
		map<studentid, double> summation_perPerson;
		//vector<double> summation_perPerson;
		//
		Ptr<CustomFaceRecognizer> cusmo ;

		//
		int _nquery;
		//int _npeople;

		/**
		*  Order the distances within vector in ascending order.
		*  Usually to see which distance is the least and owned by whom.
		*/
		multimap<double, int> order_dist(map<int, vector<double>> dist){
			// container for distance ASC ordered
			multimap<double, int> dist_ordered;

			// iterate through all dist to reorganize it to multimap, small in front first
			for (map<int, vector<double>>::iterator iter = dist.begin(); iter != dist.end(); iter++){
				vector<double> cp_vect_dist = iter->second;
				int cp_label = iter->first;
				for (double d : cp_vect_dist){
					dist_ordered.insert(pair<double, int>(d, cp_label));
				}
			}
			return dist_ordered;
		}

	public:
		static const int MODE_MIN_DIST	= 0;
		static const int MODE_INV_DIST	= 1;
		static const int MODE_Q1		= 2;
		static const int MODE_Q2		= 3;
		static const int MODE_MEAN		= 4;
		static const int MODE_INV_ONLY	= 5;
		static const int MODE_RANK_ONLY	= 6;
		static const int MODE_NEG		= 7;
		static const int MODE_VOTE		= 8;
		static const int MODE_KRAW_DIST	= 9;
		static const int MODE_INV_DIST_5	= 10;
		static const int MODE_INV_DIST_15	= 11;
		static const int MODE_INV_ONLY_5	= 12;
		static const int MODE_INV_ONLY_15	= 13;
		static const int MODE_NEG_5			= 14;
		static const int MODE_NEG_15		= 15;
		static const int MODE_OUTER_MIN		= 16;
		static const int N_MODE = 17;
		// current mode
		int mode;

		multi_prediction(){
			cusmo = createCustomFisherFaceRecognizer();
			_nquery = 0;
			mode = MODE_MIN_DIST;
		}
		~multi_prediction() {}

		static String modeName(int mode){
			switch (mode){
			case MODE_MIN_DIST: //0
				return "Minimum distance          "; 
			case MODE_INV_DIST: //1
				return "Inverse distance and Rank "; 
			case MODE_Q1:       //2
				return "Quartile 1                "; 
			case MODE_Q2:       //3
				return "Quartile 2 (Median)       ";
			case MODE_MEAN:     //4
				return "Mean                      "; 
			case MODE_INV_ONLY: //5
				return "Inverse distance          "; 
			case MODE_RANK_ONLY: //6
				return "Rank                      "; 
			case MODE_NEG:       //7
				return "Reward - distance         "; 
			case MODE_VOTE:      //8
				return "Vote                      "; 
			case MODE_KRAW_DIST: //9
				return "Raw ?????????? ?????????? "; 
			case MODE_INV_DIST_5:			//10
				return "Inv-rank k=15             "; 
			case MODE_INV_DIST_15:			//11
				return "Inv-rank k=30             "; 
			case MODE_INV_ONLY_5:			//12
				return "Inv-only k=15             "; 
			case MODE_INV_ONLY_15:			//13
				return "Inv-only k=30             "; 
			case MODE_NEG_5:				//14
				return "D - d    k=15             "; 
			case MODE_NEG_15:				//15
				return "D - d    k=30             "; 
			case MODE_OUTER_MIN:			//16
				return "Outer Vote Min            "; 
			}
			return "";
		}
		// return comparation of the mode
		// true if comparation is greater which mean, the biggest point is the prediction
		// false if smallest is better
		static bool modeCompare(int mode){
			switch (mode){
			case MODE_MIN_DIST:		//0: //"Min distance"
				return false; 
			case MODE_INV_DIST:		//1: //"Inv-rank Reward"
				return true;
			case MODE_Q1:			//2: //"Quartile 1"
				return false;
			case MODE_Q2:			//3: //"Quartile 2 (Median)"
				return false;
			case MODE_MEAN:			//4: //"Mean"
				return false; 
			case MODE_INV_ONLY:		//5: //"Inverse as Reward"
				return true; 
			case MODE_RANK_ONLY:	//6: //"Rank as Reward"
				return true; 
			case MODE_NEG:			//7: //"Const - d as Reward"
				return true; 
			case MODE_VOTE:			//8: //"Simple Voting"
				return true; 
			case MODE_KRAW_DIST:	//9: //"Raw Distance"
				return true;

			case MODE_INV_DIST_5:	//10: //"Inv-rank Reward k=5"
				return true; 
			case MODE_INV_DIST_15:	//11: //"Inv-rank Reward k=10"
				return true; 
			case MODE_INV_ONLY_5:	//12: //"Inv-only k=5   "; 
				return true;
			case MODE_INV_ONLY_15:  //13: //"Inv-only k=15   "; 
				return true;
			case MODE_NEG_5:		//14: //"D - d    k=5   "; 
				return true;
			case MODE_NEG_15:		//15: //"D - d    k=15  "; 
				return true;
			case MODE_OUTER_MIN:	//16: //"Outer Vote Min "; 
				return true;
			}
			return false;
		}

		void train(InputArray src, InputArray _lbls){
			cusmo->train(src, _lbls);
			Mat labels = _lbls.getMat();
			vector<int> ll;
			for(int i = 0; i < labels.total(); i++) {
				ll.push_back(labels.at<int>(i));
			}
			// get the number of unique classes
			labelList = remove_dups(ll);
			int _npeople = labelList.size();

			for (int i=0; i < _npeople; i++){
				summation_perPerson.insert(pair<studentid, double>(labelList.at(i), 0));
			}
		}

		void addImageQueries(vector<Mat> queries){
			for (Mat img : queries){
				addImageQuery(img);					
			}
		}

		int getNQuery(){
			return _nquery;
		}

		void addImageQuery(Mat imgQuery){
			map<int, vector<double>> all_dist;
			cusmo->allDistances(imgQuery, all_dist);
			
			// container for distance ASC ordered
			multimap<double, int> dist_ordered = order_dist(all_dist);
			multimap<double, int>::iterator it = dist_ordered.begin();
			double min_dist = it->first;
			int min_lbl = it->second;

			//cout << min_dist << endl;
			if (min_dist < 1000){

				map<studentid, double> extracted_score;
				if (mode == MODE_MIN_DIST){
					extracted_score = add_min_distance(all_dist);
				}
				else if (mode == MODE_INV_DIST) {
					extracted_score = add_inv_distance(all_dist, 30);
				}
				else if (mode == MODE_Q1) {
					extracted_score = add_q1(all_dist);
				}
				else if (mode == MODE_Q2) {
					extracted_score = add_median(all_dist);
				}
				else if (mode == MODE_MEAN){
					extracted_score = add_mean(all_dist);
				}
				else if (mode == MODE_INV_ONLY){
					extracted_score = add_inv_only(all_dist, 30);
				}
				else if (mode == MODE_RANK_ONLY){
					extracted_score = add_rank_only(all_dist);
				}
				else if (mode == MODE_NEG){
					extracted_score = add_neg_dist(all_dist);
				}
				else if (mode == MODE_VOTE) {
					extracted_score = add_vote(all_dist, 10);
				}
				else if (mode == MODE_KRAW_DIST) {
					extracted_score = add_kraw_dist(all_dist, 10);
				}
				else if (mode == MODE_INV_DIST_5) {
					extracted_score = add_inv_distance(all_dist, 15);
				}
				else if (mode == MODE_INV_DIST_15) {
					extracted_score = add_inv_distance(all_dist, 30);
				}
				else if (mode == MODE_INV_ONLY_5){
					extracted_score = add_inv_only(all_dist, 15);
				}
				else if (mode == MODE_INV_ONLY_15){
					extracted_score = add_inv_only(all_dist, 30);
				}
				else if (mode == MODE_NEG_5){
					extracted_score = add_neg_dist(all_dist, 15);
				}
				else if (mode == MODE_NEG_15){
					extracted_score = add_neg_dist(all_dist, 30);
				}
				else if (mode == MODE_OUTER_MIN){
					extracted_score = add_outer_vote_min(all_dist);
				}
				// add extracted_score to summation_points
				for (it_mapv iter = all_dist.begin(); iter != all_dist.end(); ++iter){
					int cp_label = iter->first;
					summation_perPerson[cp_label] += extracted_score[cp_label];
				}

				_nquery++;
			}
		}

		/** 
		*  Collect minimum value from each studentid.
		*/
		map<studentid, double>  add_outer_vote_min(map<studentid, vector<double>> dist){
			// initialize vote to zero
			map<studentid, double> thisvote;
			for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
				int cp_label = iter->first;
				thisvote.insert(pair<studentid, double>(cp_label, 0));
			}

			// iterate through all dist, find global minimum
			double global_min_dist = DBL_MAX;
			double global_min_dist_idx = -1;
			for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
				int cp_label = iter->first;
				vector<double> cp_vect_dist = iter->second;
				for (double d : cp_vect_dist){
					if (d < global_min_dist){
						global_min_dist = d;
						global_min_dist_idx = cp_label;
					}
				}
			}
			// vote one for winner of this query
			thisvote.at(global_min_dist_idx) = 1;
			return thisvote;
		}

		/** 
		*  Collect minimum value from each studentid.
		*/
		map<studentid, double>  add_min_distance(map<studentid, vector<double>> dist){
			// initialize minimum value as large as possible
			map<studentid, double> minClassDistance;
			for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
				int cp_label = iter->first;
				minClassDistance.insert(pair<studentid, double>(cp_label, DBL_MAX));
			}

			// iterate through all dist, for each studentid (person), find minimum
			for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
				int cp_label = iter->first;
				vector<double> cp_vect_dist = iter->second;
				double cp_min_dist = DBL_MAX;
				for (double d : cp_vect_dist){
					if (d < cp_min_dist) cp_min_dist = d;
				}
				minClassDistance.at(cp_label) = cp_min_dist;
			}
			return minClassDistance;
		}

		map<studentid, double> add_q1(map<studentid, vector<double>> dist){
			map<studentid, double>  q1Distance;
			// iterate through all dist, for each key map (person), find quatile 1
			for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
				int cp_label = iter->first;
				vector<double> cp_vect_dist = iter->second;

				//sort the vector
				std::sort(cp_vect_dist.begin(), cp_vect_dist.end());

				//find q1
				int vsize = (int) cp_vect_dist.size();
				int r = vsize % 4;
				int k = vsize / 4;
				double quartile_1 = 0;

				switch (r){
				case 0:
					quartile_1 = (cp_vect_dist.at(k) + cp_vect_dist.at(k-1)) / 2;
					break;
				case 1:
					quartile_1 = 0.25 * cp_vect_dist.at(k) + 0.75 * cp_vect_dist.at(k-1);
					break;
				case 2:
					quartile_1 = (cp_vect_dist.at(k) + cp_vect_dist.at(k-1)) / 2;
					break;
				case 3:
					quartile_1 = 0.75 * cp_vect_dist.at(k) + 0.25 * cp_vect_dist.at(k-1);
					break;
				}

				q1Distance.insert(pair<studentid, double>(cp_label, quartile_1));
			}
			return q1Distance;
		}

		map<studentid, double> add_median(map<studentid, vector<double>> dist){
			map<studentid, double>  medianDistance;
			// iterate through all dist, for each key map (person), find median of each vector
			for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
				int cp_label = iter->first;
				vector<double> cp_vect_dist = iter->second;

				//sort the vector
				std::sort(cp_vect_dist.begin(), cp_vect_dist.end());

				//find median
				int vsize = (int) cp_vect_dist.size();
				int r = vsize % 2;
				int k = vsize / 2;
				double median = 0;

				switch (r){
				case 0:
					median = (cp_vect_dist.at(k) + cp_vect_dist.at(k-1)) / 2;
					break;
				case 1:
					median = cp_vect_dist.at(k);
					break;
				}

				medianDistance.insert(pair<studentid, double>(cp_label, median));
			}
			return medianDistance;
		}

		map<studentid, double> add_mean(map<studentid, vector<double>> dist){
			map<studentid, double>  meanDistance;
			// iterate through all dist, for each key map (person), find minimum
			for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
				int cp_label = iter->first;
				vector<double> cp_vect_dist = iter->second;

				double sum = std::accumulate(cp_vect_dist.begin(), cp_vect_dist.end(), 0.0);
				double mean = sum / cp_vect_dist.size();

				meanDistance.insert(pair<studentid, double>(cp_label, mean));
			}
			return meanDistance;
		}

		map<studentid, double> add_inv_distance(map<studentid, vector<double>> dist, int krank = 10){
			map<studentid, double>  inv_dist_scores;
			// initialization of scores as zero (student has no knn)
			for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
				int cp_label = iter->first;
				inv_dist_scores.insert(pair<studentid, double>(cp_label, 0));
			}

			// container for distance ASC ordered
			multimap<double, int> dist_ordered = order_dist(dist);

			int weight = krank;
			// extract top 10 distance and compute points based on inverse distance and weight (10 - rank + 1)
			for (multimap<double, int>::iterator it = dist_ordered.begin(); it != dist_ordered.end(); ++it){
				double d = it->first;
				int l = it->second;
				double p = weight * 1000 / d;
				inv_dist_scores.at(l) += p;
				// Next
				weight--;
				if (weight == 0) break;
			}

			return inv_dist_scores;
		}

		map<studentid, double> add_neg_dist(map<studentid, vector<double>> dist, int krank = 10){
			map<studentid, double>  neg_dist;
			const int DEF_POINT = 5000;
			// initialization of scores as zero (student has no knn)
			for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
				int cp_label = iter->first;
				neg_dist.insert(pair<studentid, double>(cp_label, 0));
			}

			// container for distance ASC ordered
			multimap<double, int> dist_ordered = order_dist(dist);

			int weight = krank;
			// extract top 10 distance and compute points based on negative
			for (multimap<double, int>::iterator it = dist_ordered.begin(); it != dist_ordered.end(); ++it){
				double d = it->first;
				int l = it->second;
				double p = DEF_POINT - d;
				neg_dist.at(l) += p;
				// Next
				weight--;
				if (weight == 0) break;
			}

			return neg_dist;
		}


		map<studentid, double> add_inv_only(map<studentid, vector<double>> dist, int krank = 10){
			map<studentid, double>  inv_dist_scores;
			// initialization of scores as zero (student has no knn)
			for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
				int cp_label = iter->first;
				inv_dist_scores.insert(pair<studentid, double>(cp_label, 0));
			}

			// container for distance ASC ordered
			multimap<double, int> dist_ordered = order_dist(dist);

			int weight = krank;
			// extract top 10 distance and compute points based on inverse distance
			for (multimap<double, int>::iterator it = dist_ordered.begin(); it != dist_ordered.end(); ++it){
				double d = it->first;
				int l = it->second;
				double p = 1000 / d;
				inv_dist_scores.at(l) += p;
				// Next
				weight--;
				if (weight == 0) break;
			}

			return inv_dist_scores;
		}

		map<studentid, double> add_rank_only(map<studentid, vector<double>> dist, int krank = 10){
			map<studentid, double>  rank_scores;
			// initialization of scores as zero (student has no knn)
			for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
				int cp_label = iter->first;
				rank_scores.insert(pair<studentid, double>(cp_label, 0));
			}

			// container for distance ASC ordered
			multimap<double, int> dist_ordered = order_dist(dist);

			int weight = krank;
			// extract top 10 distance and compute points based on rank only
			for (multimap<double, int>::iterator it = dist_ordered.begin(); it != dist_ordered.end(); ++it){
				int l = it->second;
				// Rank is deriving weight
				rank_scores.at(l) += weight;
				// Next
				weight--;
				if (weight == 0) break;
			}

			return rank_scores;
		}

		map<studentid, double> add_vote(map<studentid, vector<double>> dist, int krank = 10){
			map<studentid, double>  votes;
			// initialization of scores as zero vote (student has no knn)
			for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
				int cp_label = iter->first;
				votes.insert(pair<studentid, double>(cp_label, 0));
			}

			// container for distance ASC ordered
			multimap<double, int> dist_ordered = order_dist(dist);

			int weight = krank;
			// extract top 10 distance 
			for (multimap<double, int>::iterator it = dist_ordered.begin(); it != dist_ordered.end(); ++it){
				int l = it->second;
				// VOTE = 1
				votes.at(l) += 1;
				weight--;
				if (weight == 0) break;
			}
			return votes;
		}

		map<studentid, double> add_kraw_dist(map<studentid, vector<double>> dist, int krank = 10){
			map<studentid, double>  sumraw;
			// initialization of scores as zero vote (student has no knn)
			for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
				int cp_label = iter->first;
				sumraw.insert(pair<studentid, double>(cp_label, 0));
			}

			// container for distance ASC ordered
			multimap<double, int> dist_ordered = order_dist(dist);

			int weight = krank;
			double least_raw_dist = DBL_MAX;
			// extract top 10 distance
			for (multimap<double, int>::iterator it = dist_ordered.begin(); it != dist_ordered.end(); ++it){
				int l = it->second;
				double raw_d = it->first;
				//Raw Distance as points
				sumraw.at(l) += raw_d;
				weight--;
				if (weight == 0) {
					least_raw_dist = raw_d * 2;
					break;
				}
			}
			// for student without any knn, put penalty
			for (map<studentid, double>::iterator it = sumraw.begin(); it != sumraw.end(); ++it){
				if (it->second == 0)
					sumraw.at(it->first) = least_raw_dist;
				//if (sumraw.at(i) == 0) sumraw.at(i) = least_raw_dist;
			}
			return sumraw;
		}

		void resetQuery(){
			summation_perPerson.clear();
			for (int i=0; i < labelList.size(); i++){
				summation_perPerson.insert(pair<studentid, double>(labelList.at(i), 0));
			}
			_nquery=0;
		}

		int predict(){
			bool grCompare = modeCompare(mode);
			// decide whether to predict by max score or min score
			if (grCompare){
				// sort biggest first, DESC
				multimap<double, int, std::greater<double>> sorted;
				for (map<int, double>::iterator it = summation_perPerson.begin(); it != summation_perPerson.end(); ++it){
					int label = it->first;
					double distn = it->second;
					//insert, biggest first
					sorted.insert( std::pair<double, int>(distn, label));
				}
				// the biggest points is in the front of map
				multimap<double, int>::iterator it = sorted.begin();
				// return label with biggest distance
				return it->second;
			}
			else {
				// sort smallest first, ASC
				multimap<double, int> sorted;
				for (map<int, double>::iterator it = summation_perPerson.begin(); it != summation_perPerson.end(); ++it){
					int label = it->first;
					double distn = it->second;
					//insert, smallest first
					sorted.insert( std::pair<double, int>(distn, label));
				}
				// the smallest points is in the front of map
				multimap<double, int>::iterator it = sorted.begin();
				// return label with smallest distance
				return it->second;
			}
		}

		bool existInRank(int maxRank, int realLabel){
			bool grCompare = modeCompare(mode);
			// decide whether to predict by max score or min score
			if (grCompare){
				// sort biggest first, DESC
				multimap<double, int, std::greater<double>> sorted;
				for (map<int, double>::iterator it = summation_perPerson.begin(); it != summation_perPerson.end(); ++it){
					int label = it->first;
					double distn = it->second;
					//insert, biggest first
					sorted.insert( std::pair<double, int>(distn, label));
				}
				// the biggest points is in the front of map
				multimap<double, int>::iterator it = sorted.begin();

				bool found = false;
				for (int i = 1; i <= maxRank; ++i){
					int curLabel = it->second;
					if (curLabel == realLabel) found = true;
					it++;
				}
				return found;
			}
			else {
				// sort smallest first, ASC
				multimap<double, int> sorted;
				for (map<int, double>::iterator it = summation_perPerson.begin(); it != summation_perPerson.end(); ++it){
					int label = it->first;
					double distn = it->second;
					//insert, smallest first
					sorted.insert( std::pair<double, int>(distn, label));
				}
				// the smallest points is in the front of map
				multimap<double, int>::iterator it = sorted.begin();

				bool found = false;
				for (int i = 1; i <= maxRank; ++i){
					int curLabel = it->second;
					if (curLabel == realLabel) found = true;
					it++;
				}
				return found;
			}
			return false;
		}

		
		vector<int> returnRanked(int maxRank){
			bool grCompare = modeCompare(mode);
			// decide whether to predict by max score or min score
			if (grCompare){
				// sort biggest first, DESC

				multimap<double, int, std::greater<double>> sorted;
				for (map<int, double>::iterator it = summation_perPerson.begin(); it != summation_perPerson.end(); ++it){
					int label = it->first;
					double distn = it->second;
					//insert, biggest first
					sorted.insert( std::pair<double, int>(distn, label));
				}
				// the biggest points is in the front of map
				multimap<double, int>::iterator it = sorted.begin();
				

				vector<int> rankedLabel; int rank = 0;
				for (map<double, int>::iterator it = sorted.begin(); it != sorted.end(); ++it){
					int curLabel = it->second;
					
					//cout << " (" << curLabel << "):" << it->first << " ";
					rankedLabel.push_back(curLabel);
					rank++;
					if (rank >= maxRank) break;
				}

				//cout << endl;
				//vector<int> rankedLabel;
				//bool found = false;
				//cout << "sorted.size() " << sorted.size() << endl;
				//for (int i = 0; i < maxRank && i < sorted.size(); ++i){
				//	int curLabel = it->second;
				//	cout << "curLabel " << curLabel;
				//	rankedLabel.push_back(curLabel);
				//	it++;
				//}
				//cout << "rankedLabel.size() " << rankedLabel.size() << endl;

				return rankedLabel;
			}
			else {
				// sort smallest first, ASC
				multimap<double, int> sorted;
				for (map<int, double>::iterator it = summation_perPerson.begin(); it != summation_perPerson.end(); ++it){
					int label = it->first;
					double distn = it->second;
					//insert, smallest first
					sorted.insert( std::pair<double, int>(distn, label));
				}
				//// the smallest points is in the front of map
				//multimap<double, int>::iterator it = sorted.begin();
				//vector<int> rankedLabel;
				//bool found = false;
				//cout << "sorted.size() " << sorted.size() << endl;
				//for (int i = 0; i < maxRank && i < sorted.size(); ++i){
				//	int curLabel = it->second;
				//	cout << "curLabel " << curLabel;
				//	rankedLabel.push_back(curLabel);
				//	it++;
				//}
				//cout << "rankedLabel.size() " << rankedLabel.size() << endl;

				
				vector<int> rankedLabel; int rank = 0;
				for (map<double, int>::iterator it = sorted.begin(); it != sorted.end(); ++it){
					int curLabel = it->second;
					rankedLabel.push_back(curLabel);
					rank++;
					if (rank >= maxRank) break;
				}


				return rankedLabel;
			}
			return vector<int>();
		}


		map<studentid, double> getSummation(){
			return summation_perPerson;
		}

		bool predictThree(int realLabel){
			bool grCompare = modeCompare(mode);
			// decide whether to predict by max score or min score
			if (grCompare){
				// sort biggest first, DESC
				multimap<double, int, std::greater<double>> sorted;
				for (map<int, double>::iterator it = summation_perPerson.begin(); it != summation_perPerson.end(); ++it){
					int label = it->first;
					double distn = it->second;
					//insert, biggest first
					sorted.insert( std::pair<double, int>(distn, label));
				}
				// the biggest points is in the front of map
				multimap<double, int>::iterator it = sorted.begin();
				//first
				int firstPrediction = it->second;
				//second
				it++;
				int secondPrediction = it->second;
				//third
				it++;
				int thirdPrediction = it->second;

				// check whether one of them is correct
				return (firstPrediction == realLabel) ||  (secondPrediction == realLabel) ||  (thirdPrediction == realLabel) ;
			}
			else {
				// sort smallest first, ASC
				multimap<double, int> sorted;
				for (map<int, double>::iterator it = summation_perPerson.begin(); it != summation_perPerson.end(); ++it){
					int label = it->first;
					double distn = it->second;
					//insert, smallest first
					sorted.insert( std::pair<double, int>(distn, label));
				}
				// the smallest points is in the front of map
				multimap<double, int>::iterator it = sorted.begin();
				//first
				int firstPrediction = it->second;
				//second
				it++;
				int secondPrediction = it->second;
				//third
				it++;
				int thirdPrediction = it->second;

				// check whether one of them is correct
				return (firstPrediction == realLabel) ||  (secondPrediction == realLabel) ||  (thirdPrediction == realLabel) ;
			}
			return false;
		}

	};



}