
#ifndef HELPER_H
#define HELPER_H

#include "stdafx.h"
#include "dirent.h"

// Function which is used by many files
using namespace cv;
using namespace std;

static void read_csv(const string& filename, vector<Mat>& images, vector<int>& labels, char separator = ';') {
	std::ifstream file(filename.c_str(), ifstream::in);
	if (!file) {
		string error_message = "No valid input file was given, please check the given filename.";
		CV_Error(CV_StsBadArg, error_message);
	}
	string line, path, classlabel;
	while (getline(file, line)) {
		stringstream liness(line);
		getline(liness, path, separator);
		getline(liness, classlabel);
		if(!path.empty() && !classlabel.empty()) {
			images.push_back(imread(path, 0));
			labels.push_back(atoi(classlabel.c_str()));
		}
	}
}



/**
*  This function assumse that all images within filename is all subject folders.
*  And then, the images of each subject is seperated in each subject folders.
*	The labels of subject is only integer from zero into subject_folder.size()-1
*  which is the position of the subject folder within imagesPath
*/
static void read_folder_imagesPerPerson(const string& filename, vector<vector<Mat>>& images) {
	//Open directory source and shuffle observations on each person 
	DIR *dir;
	struct dirent *ent;
	//open main folder containing folder of subjects
	if ((dir = opendir (filename.c_str())) != NULL) {
		/* read all directory*/
		while ((ent = readdir (dir)) != NULL) {
			switch (ent->d_type) {
			case DT_DIR:
				//if this is a directory and is not a parent
				if (strcmp (ent->d_name, ".") != 0 && strcmp (ent->d_name,"..") != 0){
					//open this directory [this person]
					DIR *dirPerson;
					struct dirent *entPerson;
					string personFolder = filename + "/" + ent->d_name;

					//prepare image container for this person
					vector<Mat> personImages;

					//open the directory
					if ((dirPerson = opendir (personFolder.c_str())) != NULL){
						//open the images within this person's directory 
						while ((entPerson = readdir (dirPerson)) != NULL) {
							//again if this is not a parent (!directory checking is not done)
							if (strcmp (entPerson->d_name, ".") != 0 && strcmp (entPerson->d_name,"..") != 0){
								string imageFullpath = personFolder + "/" + entPerson->d_name;
								personImages.push_back(imread(imageFullpath, 0));
							}
						}
						closedir (dirPerson);
					}

					//add into images container
					images.push_back(personImages);
				}
				break;
			}
		}
		closedir (dir);
	} else {
		/* could not open directory */
		perror ("EXIT_FAILURE");
		//return EXIT_FAILURE;
	}
}

/**
*  This function assumse that all images within filename is all subject folders.
*  And then, the images of each subject is seperated in each subject folders.
*  The labels of subject is only integer from zero into subject_folder.size()-1
*  which is the position of the subject folder within imagesPath
*/
static void read_folder_str(const string& filename, vector<string>& images, vector<int>& labels) {
	//Open directory source and shuffle observations on each person 
	DIR *dir;
	struct dirent *ent;
	int intLabel = 0;
	//open main folder containing folder of subjects
	if ((dir = opendir (filename.c_str())) != NULL) {
		/* read all directory*/
		while ((ent = readdir (dir)) != NULL) {
			switch (ent->d_type) {
			case DT_DIR:
				//if this is a directory and is not a parent
				if (strcmp (ent->d_name, ".") != 0 && strcmp (ent->d_name,"..") != 0){
					//open this directory [this person]
					DIR *dirPerson;
					struct dirent *entPerson;
					string personFolder = filename + "/" + ent->d_name;

					//open the directory
					if ((dirPerson = opendir (personFolder.c_str())) != NULL){
						//open the images within this person's directory 
						while ((entPerson = readdir (dirPerson)) != NULL) {
							//again if this is not a parent (!directory checking is not done)
							if (strcmp (entPerson->d_name, ".") != 0 && strcmp (entPerson->d_name,"..") != 0){
								string imageFullpath = personFolder + "/" + entPerson->d_name;
								//add to container
								images.push_back(imageFullpath);
								labels.push_back(intLabel);
							}
						}
						closedir (dirPerson);
					}

					//next label
					intLabel++;
				}
				break;
			}
		}
		closedir (dir);
	} else {
		/* could not open directory */
		perror ("EXIT_FAILURE");
		//return EXIT_FAILURE;
	}
}

/**
*  This function assumse that all images within filename is all subject folders and folder name is numeric.
*  And then, the images of each subject is seperated in each subject folders.
*  The labels of subject is the folder name and set as key in map.
*/
static void read_folder_map(const string& filename, map<int, vector<Mat>>& images) {
	//Open directory source and shuffle observations on each person 
	DIR *dir;
	struct dirent *ent;
	//open main folder containing folder of subjects
	if ((dir = opendir (filename.c_str())) != NULL) {
		/* read all directory*/
		while ((ent = readdir (dir)) != NULL) {
			switch (ent->d_type) {
			case DT_DIR:
				//if this is a directory and is not a parent
				if (strcmp (ent->d_name, ".") != 0 && strcmp (ent->d_name,"..") != 0){
					//open this directory [this person]
					DIR *dirPerson;
					struct dirent *entPerson;
					string personFolder = filename + "/" + ent->d_name;
					int label = atoi(ent->d_name);

					//prepare image container for this person
					vector<Mat> personImages;

					//open the directory
					if ((dirPerson = opendir (personFolder.c_str())) != NULL){
						//open the images within this person's directory 
						while ((entPerson = readdir (dirPerson)) != NULL) {
							//again if this is not a parent (!directory checking is not done)
							if (strcmp (entPerson->d_name, ".") != 0 && strcmp (entPerson->d_name,"..") != 0){
								string imageFullpath = personFolder + "/" + entPerson->d_name;
								personImages.push_back(imread(imageFullpath, 0));
							}
						}
						closedir (dirPerson);
					}

					//add into images container
					cout << label << " | ";
					images.insert(pair<int, vector<Mat>>(label, personImages));
				}
				break;
			}
		}
		closedir (dir);
	} else {
		/* could not open directory */
		perror ("EXIT_FAILURE");
		//return EXIT_FAILURE;
	}
}

/**
*  This function assumse that all images within filename is all subject folders and folder name is numeric.
*  And then, the images of each subject is seperated in each subject folders.
*  The labels of subject is the folder name and set as key in map.
*/
static void read_folder_mapQuery(const string& filename, map<int, vector<vector<Mat>>>& images) {
	//Open directory source and shuffle observations on each person 
	DIR *dir;
	struct dirent *ent;
	//open main folder containing folder of subjects
	if ((dir = opendir (filename.c_str())) != NULL) {
		/* read all directory*/
		while ((ent = readdir (dir)) != NULL) {
			switch (ent->d_type) {
			case DT_DIR:
				//if this is a directory and is not a parent
				if (strcmp (ent->d_name, ".") != 0 && strcmp (ent->d_name,"..") != 0){
					//open this directory [this person]
					DIR *dirPerson;
					struct dirent *entPerson;
					string personFolder = filename + "/" + ent->d_name;
					int label = atoi(ent->d_name);

					//prepare image container for this person
					vector<vector<Mat>> personImages;

					//open the directory
					if ((dirPerson = opendir (personFolder.c_str())) != NULL){
						//open the images within this person's directory 
						while ((entPerson = readdir (dirPerson)) != NULL) {
							switch (entPerson->d_type) {
							case DT_DIR:
								//if this is a directory and is not a parent
								if (strcmp (entPerson->d_name, ".") != 0 && strcmp (entPerson->d_name,"..") != 0){
									//open this directory [this person]
									DIR *dirSeq;
									struct dirent *entSeq;
									string seqFolder = personFolder + "/" + entPerson->d_name;

									//prepare image container for this person
									vector<Mat> seqImages;

									//open the directory
									if ((dirSeq = opendir (seqFolder.c_str())) != NULL){
										//open the images within this person's directory 
										while ((entSeq = readdir (dirSeq)) != NULL) {
											//again if this is not a parent (!directory checking is not done)
											if (strcmp (entSeq->d_name, ".") != 0 && strcmp (entSeq->d_name,"..") != 0 && entSeq->d_type == DT_REG){
												string imageFullpath = seqFolder + "/" + entSeq->d_name;
												//cout << imageFullpath << endl;
												seqImages.push_back(imread(imageFullpath, 0));
											}
										}
										closedir (dirSeq);
									}

									//add into images container
									cout << label << " | ";
									personImages.push_back(seqImages);
								}
								break;
							}
						}
						closedir (dirPerson);
					}

					//add into images container
					images.insert(pair<int, vector<vector<Mat>>>(label, personImages));
				}
				break;
			}
		}
		closedir (dir);
	} else {
		/* could not open directory */
		perror ("EXIT_FAILURE");
		//return EXIT_FAILURE;
	}
}

#endif
