
#include "stdafx.h"
#include "training_test.h"
#include "combinatorics.h"
#include <direct.h>


#include "evalMultipleQueryPrediction.h"

void trainTest(int argc, const char *argv[]){
	// For evaluating ntrain and ntest variable
	cout << "Please run with parameter 1=folder, 2=training, 3=test, 4=iteration" << endl << endl;
	cout << "Evaluation of multi query prediction." << endl;
	//for one parameter experiment
	evalMultipleQueryPrediction eve;
	eve.SET_PREFIX = "minitraite ";
	eve.SET_PRINT_PREFSIZE = false;
	
	cout << "Breaking stdev: " << eve.STDEVBREAK() << endl;

	eve.eval_random(argv[1], atoi (argv[2]), atoi (argv[3]), 80, atoi (argv[4]));
}


void sizeEval(int argc, const char *argv[]){
	// For evaluating ntrain and ntest variable
	cout << "Please run with parameter 1=folder, 2=training, 3=test, 4=IMAGE_SIZE, 5=iteration" << endl << endl;
	cout << "Evaluation of multi query prediction." << endl;
	//for one parameter experiment
	evalMultipleQueryPrediction eve;
	eve.SET_3PRED = true;
	eve.SET_PRINT_PREFSIZE = true;
	
	cout << "Breaking stdev: " << eve.STDEVBREAK() << endl;
	eve.eval_random(argv[1], atoi (argv[2]), atoi (argv[3]), atoi (argv[4]), atoi (argv[5]));
}

void onePred(int argc, const char *argv[]){
	// For evaluating ntrain and ntest variable
	cout << "Please run with parameter 1=folder, 2=training, 3=test, 4=iteration" << endl << endl;
	cout << "Evaluation of multi query prediction using 64 x 64." << endl;
	cout << "ONLY THE FIRST ONE is considered prediction." << endl;
	cout << endl;
	//for one parameter experiment
	evalMultipleQueryPrediction eve;
	eve.SET_PREFIX = "onev ";
	eve.eval_random(argv[1], atoi (argv[2]), atoi (argv[3]), 64, atoi (argv[4]));
}


void threePred(int argc, const char *argv[]){
	// For evaluating ntrain and ntest variable
	cout << "Please run with parameter 1=folder, 2=training, 3=test, 4=iteration" << endl << endl;
	cout << "Evaluation of multi query prediction using 80 x 80." << endl;
	cout << "Top three is considered." << endl;
	cout << endl;
	//for one parameter experiment
	evalMultipleQueryPrediction eve;
	eve.SET_PREFIX = "x3in1 ";
	eve.SET_3PRED = true;
	
	cout << "Breaking stdev: " << eve.STDEVBREAK() << endl;
	eve.eval_random(argv[1], atoi (argv[2]), atoi (argv[3]), 80, atoi (argv[4]));
}


void kEval(int argc, const char *argv[]){
	// For evaluating ntrain and ntest variable
	cout << "Evaluating K of KNN Value." << endl;
	cout << "Please run with parameter 1=folder, 2=training, 3=test, 4=iteration" << endl << endl;
	cout << "Evaluation of multi query prediction using 80 x 80." << endl;
	cout << endl;
	//for one parameter experiment
	evalMultipleQueryPrediction eve;
	eve.SET_PREFIX = "kEv ";
	eve.SET_3PRED = true;

	eve.modeForStdevCheck = 12;
	
	// only want to test different K
	const int NTM = 4;
	eve.N_TESTED_MODE = NTM;
	eve.tested_mode = new int [NTM];
	int listMode[NTM] = {12, 13, 14, 15};
	for (int i = 0; i < NTM; i++) {
		eve.tested_mode[i] = listMode[i];
	}

	eve.eval_random(argv[1], atoi (argv[2]), atoi (argv[3]), 80, atoi (argv[4]));
}

void allCombination(int argc, const char *argv[]){
	// For evaluating ntrain and ntest variable
	cout << "Please run with parameter 1=folder, 2=nimages per person, 3=ntrain, 4=ntest" << endl << endl;
	cout << "Evaluating All Combination Training and Test Combination." << endl;
	cout << "Evaluation of multi query prediction using 64 x 64." << endl;
	cout << endl;
	evalMultipleQueryPrediction eve;
	eve.SET_PREFIX = "all ";
	eve.SET_3PRED = false;
	
	// only want to test different K
	const int NTM = 2;
	eve.N_TESTED_MODE = NTM;
	eve.tested_mode = new int [NTM];
	int listMode[NTM] = {0, 16};
	for (int i = 0; i < NTM; i++) {
		eve.tested_mode[i] = listMode[i];
	}

	eve.eval_allcombi(argv[1], atoi (argv[2]), atoi (argv[3]), atoi (argv[4]));
}

void re(String folder, int size){
	evalMultipleQueryPrediction eve;
	vector<vector<Mat>> images_ilu = eve.loadImages(folder, false, Size(size, size));

	stringstream sss;
	sss << folder <<" - "<< size;
	String target = sss.str();
	_mkdir(target.c_str());

	stringstream ss;
	
	int iv = 0;
	for (vector<Mat> vmat : images_ilu){
		int im = 0;
		for (Mat m : vmat){
			ss.str(std::string());
			ss << target << "/" << iv ;
			String ft = ss.str();
			_mkdir(ft.c_str()); 
			ss  << "/" << im << ".jpg";
			imwrite(ss.str(), m);
			im++;
		}
		iv++;
	}
}

void custom(int argc, const char *argv[]){
	// For evaluating ntrain and ntest variable
	cout << "Evaluating K of KNN Value." << endl;
	cout << "Please run with parameter 1=folder, 2=training, 3=test, 4=iteration, 5=logPrefix, 6=stdevBreak" << endl << endl;
	cout << "Evaluation of multi query prediction using 80 x 80." << endl;
	cout << endl;
	//for one parameter experiment
	evalMultipleQueryPrediction eve;
	eve.SET_PREFIX = argv[5];
	eve.CSTM_DEVBREAK = atof (argv[6]);
	eve.SET_3PRED = true;
	
	cout << "Breaking stdev: " << eve.STDEVBREAK() << endl;
	eve.eval_random(argv[1], atoi (argv[2]), atoi (argv[3]), 80, atoi (argv[4]));
}

void online(int argc, const char *argv[]){
	//for one parameter experiment
	evalMultipleQueryPrediction eve;
	eve.SET_PRINT_PREFSIZE = true;
	const int NTM = 10;
	eve.N_TESTED_MODE = NTM;
	eve.tested_mode = new int [NTM];
	int listMode[NTM] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	for (int i = 0; i < NTM; i++) {
		eve.tested_mode[i] = listMode[i];
	}

	if (argc == 4)
		eve.eval_online(argv[1], argv[2], atoi(argv[3]));
	else if (argc == 5)
		eve.eval_online(argv[1], argv[2], atoi(argv[3]), atoi(argv[4]));
}

int main(int argc, const char *argv[]) {
	//int n = atoi (argv[1]), k = atoi (argv[2]);
	//vector<int> people;
	//for (int i = 0; i < n; ++i) { people.push_back(i+1); }
	//Combinatorics coco(people, k);

	////for (int t = 0; t < 25; t++)
	/////	coco.pretty_print(coco.next_combination());

	//vector<int> c;
	//do {
	//	c = coco.next_combination();
	//	coco.pretty_print(c);
	//}
	//while(!c.empty());


	//int nexp = atoi (argv[2]);
	//int ntrn = atoi (argv[3]);
	//numeralCrossValOnline(ntrn, argv[1], nexp);
	//multipleQueryTest(ntrn, argv[1], nexp);
	//multipleQueryEvaluate(ntrn, argv[1], nexp);
	//multipleQueryEvaluate_stairs(atoi (argv[2]), atoi (argv[3]), atoi (argv[4]), argv[1]);

	// Lala.exe dataset ntrain ntest nexp
	//multipleQueryEvaluate2(argv[1], atoi (argv[2]), atoi (argv[3]), atoi (argv[4]));
	
	// PREDICTION WITH RANDOM SEPARATION
	//threePred(argc, argv);
	//trainTest(argc, argv);
	//sizeEval(argc, argv);
	//kEval(argc, argv);
	//allCombination(argc, argv);

	//re(argv[1], atoi(argv[2]));


	online(argc, argv);
}

